package com.magycbytes.opensubtitlelibrary.Callbacks;

import android.util.Log;

import com.magycbytes.opensubtitlelibrary.Models.Network.Member;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;
import com.magycbytes.opensubtitlelibrary.Models.Network.Value;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by alexandru on 6/26/16.
 */

public class SearchSubtitleCallback extends BasicCallback {

    private OnSearchSubtitle mOnSearchSubtitleListener;

    public SearchSubtitleCallback(OnSearchSubtitle onSearchSubtitleListener) {
        if (onSearchSubtitleListener == null) {
            throw new IllegalArgumentException("on search listener can't be null");
        }
        mOnSearchSubtitleListener = onSearchSubtitleListener;
    }


    @Override
    public void onResponse(Call<MethodResponse> call, Response<MethodResponse> response) {
        Log.i(this.toString(), "Get a response for searching subtitles, parsing it");
        List<Value> values = getValues(response);
        if (values == null) {
            Log.e(this.toString(), "Not found values in response");
            mOnSearchSubtitleListener.error();
            return;
        }

        List<Subtitle> resultSubtitles = new ArrayList<>(values.size());

        for (int i = 0; i < values.size(); ++i) {
            resultSubtitles.add(parseMembers(values.get(i).getStruct().getMembers()));
        }
        mOnSearchSubtitleListener.subtitlesFound(resultSubtitles);
    }

    private Subtitle parseMembers(List<Member> members) {
        Subtitle subtitle = new Subtitle();

        for (int j = 0; j < members.size(); ++j) {

            String valueString = members.get(j).getValue().getString();
            switch (members.get(j).getName()) {
                case "SubFileName":
                    subtitle.setSubtitleFileName(valueString);
                    break;
                case "IDSubtitleFile":
                    subtitle.setIdSubtitleFile(valueString);
                    break;
                case "ISO639":
                    subtitle.setLanguageIso639(valueString);
                    break;
                case "LanguageName":
                    subtitle.setLanguageLongName(valueString);
                    break;
                case "SubEncoding":
                    subtitle.setSubtitleEncoding(valueString);
                    break;
            }
        }
        return subtitle;
    }

    @Override
    public void onFailure(Call<MethodResponse> call, Throwable t) {
        t.printStackTrace();
        if (t.getCause() instanceof SocketTimeoutException) {
            Log.e(this.toString(), "Slow network connection");
            mOnSearchSubtitleListener.error();
            return;
        }
        Log.i(this.toString(), "Error on searching subtitles");
        String errorMessage;
        if (t.getMessage() != null) {
            errorMessage = t.getMessage();
        } else {
            errorMessage = "Unknown error";
        }
        Log.e(this.toString(), errorMessage);
        mOnSearchSubtitleListener.error();
    }

    public interface OnSearchSubtitle extends CallbackError{
        void subtitlesFound(List<Subtitle> subtitles);
    }
}
