package com.magycbytes.opensubtitlelibrary.Models.UserResult;


/**
 * Created by alexandru on 6/26/16.
 */

public class Subtitle implements Comparable<Subtitle> {

    private String mSubtitleFileName;
    private String mLanguageIso639;
    private String mLanguageLongName;
    private String mIdSubtitleFile;
    private String mSubtitleEncoding;
    private boolean mIsDownloaded;

    public String getSubtitleFileName() {
        return mSubtitleFileName;
    }

    public void setSubtitleFileName(String subtitleFileName) {
        mSubtitleFileName = subtitleFileName;
    }

    public String getLanguageIso639() {
        return mLanguageIso639;
    }

    public void setLanguageIso639(String languageIso639) {
        mLanguageIso639 = languageIso639;
    }

    public String getLanguageLongName() {
        return mLanguageLongName;
    }

    public void setLanguageLongName(String languageLongName) {
        mLanguageLongName = languageLongName;
    }

    public String getIdSubtitleFile() {
        return mIdSubtitleFile;
    }

    public void setIdSubtitleFile(String idSubtitleFile) {
        mIdSubtitleFile = idSubtitleFile;
    }

    public String getSubtitleEncoding() {
        return mSubtitleEncoding;
    }

    public void setSubtitleEncoding(String subtitleEncoding) {
        mSubtitleEncoding = subtitleEncoding;
    }

    @Override
    public String toString() {
        return mLanguageLongName;
    }

    @Override
    public int compareTo(Subtitle subtitle) {
        return mLanguageLongName.compareTo(subtitle.getLanguageLongName());
    }

    public boolean isDownloaded() {
        return mIsDownloaded;
    }

    public void setDownloaded() {
        mIsDownloaded = true;
    }
}
