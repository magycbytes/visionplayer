package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by alexandru on 6/25/16.
 */

@SuppressWarnings("unused")
@Root(name = "member")
public class Member {

    @Element(name = "name")
    private String mName;

    @Element(name = "value")
    private Value mValue;

    public Member() {

    }

    public Member(String name, Value value) {
        mName = name;
        mValue = value;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Value getValue() {
        return mValue;
    }

    public void setValue(Value value) {
        mValue = value;
    }
}
