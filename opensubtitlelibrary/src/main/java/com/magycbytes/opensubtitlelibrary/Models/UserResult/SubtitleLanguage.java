package com.magycbytes.opensubtitlelibrary.Models.UserResult;

import android.content.Context;

import com.magycbytes.opensubtitlelibrary.R;

/**
 * Created by alexandru on 6/25/16.
 */

public class SubtitleLanguage implements Comparable<SubtitleLanguage> {

    private String mLanguageName;
    private String mIso639;

    public SubtitleLanguage() {
    }

    public SubtitleLanguage(String iso639, Context context) {
        mLanguageName = context.getString(R.string.text_all);
        mIso639 = iso639;
    }

    private String getLanguageName() {
        return mLanguageName;
    }

    public void setLanguageName(String languageName) {
        mLanguageName = languageName;
    }

    public String getIso639() {
        return mIso639;
    }

    public void setIso639(String iso639) {
        mIso639 = iso639;
    }

    @Override
    public String toString() {
        return mLanguageName;
    }

    @Override
    public int compareTo(SubtitleLanguage subtitleLanguage) {
        return mLanguageName.compareTo(subtitleLanguage.getLanguageName());
    }
}
