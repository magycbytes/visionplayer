package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by alexandru on 6/25/16.
 */

@SuppressWarnings("unused")
@Root(name = "param")
public class Param {

    @Element(name = "value")
    private Value mValue;

    public Param() {

    }

    public Param(Value value) {
        mValue = value;
    }

    public Value getValue() {
        return mValue;
    }

    public void setValue(Value value) {
        mValue = value;
    }
}
