package com.magycbytes.opensubtitlelibrary.Models.Network;

import com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.DoubleConverter;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.convert.Convert;

/**
 * Created by alexandru on 6/25/16.
 */
@SuppressWarnings("unused")
@Root(name = "value")
public class Value {

    @Element(name = "string", required = false)
    private String mString;

    @Element(name = "int", required = false)
    private Integer mSubtitleId;

    @Element(name = "array", required = false)
    private Array mArray;

    @Element(name = "double", required = false)
    @Convert(DoubleConverter.class)
    private Double mDouble;

    @Element(name = "struct", required = false)
    private Struct mStruct;

    public Value() {

    }

    public Value(String string) {
        mString = string;
    }

    public Value(int subtitleId) {
        mSubtitleId = subtitleId;
    }

    public Value(Array array) {
        mArray = array;
    }

    public Value(double aDouble) {
        mDouble = aDouble;
    }

    public Value(Struct struct) {
        mStruct = struct;
    }

    public String getString() {
        return mString;
    }

    public void setString(String string) {
        mString = string;
    }

    public int getSubtitleId() {
        return mSubtitleId;
    }

    public void setSubtitleId(int subtitleId) {
        mSubtitleId = subtitleId;
    }

    public Array getArray() {
        return mArray;
    }

    public void setArray(Array array) {
        mArray = array;
    }

    public double getDouble() {
        return mDouble;
    }

    public void setDouble(double aDouble) {
        mDouble = aDouble;
    }

    public Struct getStruct() {
        return mStruct;
    }

    public void setStruct(Struct struct) {
        mStruct = struct;
    }
}
