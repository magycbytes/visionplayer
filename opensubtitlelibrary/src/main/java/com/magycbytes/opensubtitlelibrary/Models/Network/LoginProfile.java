package com.magycbytes.opensubtitlelibrary.Models.Network;

/**
 * Created by acebotari on 4/25/16.
 */
public class LoginProfile {
    private final String name;
    private final String password;
    private final String uiLanguage;  // 2 symbols
    private final String clientName;

    public LoginProfile(String uiLanguage) {
        this.name = "";
        this.password = "";
        this.uiLanguage = uiLanguage;
        this.clientName = "visionplayer";
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getUiLanguage() {
        return uiLanguage;
    }

    public String getClientName() {
        return clientName;
    }
}
