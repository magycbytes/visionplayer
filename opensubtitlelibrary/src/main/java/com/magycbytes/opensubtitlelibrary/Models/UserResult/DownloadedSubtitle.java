package com.magycbytes.opensubtitlelibrary.Models.UserResult;

/**
 * Created by alexandru on 6/25/16.
 */

public class DownloadedSubtitle {
    private final String mSubtitleContent;

    public DownloadedSubtitle(String subtitleContent) {
        mSubtitleContent = subtitleContent;
    }

    public String getSubtitleContent() {
        return mSubtitleContent;
    }

}
