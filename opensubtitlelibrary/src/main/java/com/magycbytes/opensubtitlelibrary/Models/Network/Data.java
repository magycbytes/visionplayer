package com.magycbytes.opensubtitlelibrary.Models.Network;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by alexandru on 6/25/16.
 */

@SuppressWarnings("unused")
public class Data {

    @ElementList(inline = true, required = false)
    private List<Value> mValues;

    public Data() {

    }

    public Data(List<Value> values) {
        mValues = values;
    }


    public List<Value> getValues() {
        return mValues;
    }

    public void setValues(List<Value> values) {
        mValues = values;
    }
}
