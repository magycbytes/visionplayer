package com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator;

import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

/**
 * Created by alexandru on 6/26/16.
 */

@SuppressWarnings("unused")
public class DoubleConverter implements Converter<Double> {

    @Override
    public Double read(InputNode node) throws Exception {
        return Double.valueOf(node.getValue());
    }

    @Override
    public void write(OutputNode node, Double value) throws Exception {
        long longValue = (long) value.doubleValue();
        node.setValue(String.valueOf(longValue));
    }
}
