package com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator;

/**
 * Created by alexandru on 6/26/16.
 */

public class SearchSubtitleParameters {
    private final String mAccessToken;
    private final String mSubtitleLanguage;
    private final String mMovieHash;
    private final double mMovieSize;

    public SearchSubtitleParameters(String accessToken, String subtitleLanguage, String movieHash, double movieSize) {
        mAccessToken = accessToken;
        mSubtitleLanguage = subtitleLanguage;
        mMovieHash = movieHash;
        mMovieSize = movieSize;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public String getSubtitleLanguage() {
        return mSubtitleLanguage;
    }

    public String getMovieHash() {
        return mMovieHash;
    }

    public double getMovieSize() {
        return mMovieSize;
    }

}
