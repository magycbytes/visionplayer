package com.magycbytes.opensubtitlelibrary.Network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by alexandru on 6/25/16.
 */

public class NetworkApiManager {

    public static OpenSubtitleService getApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Content-Type", "text/xml")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.opensubtitles.org")
                .client(httpClient.build())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addConverterFactory(new ToStringConverterFactory())
                .build();
        return retrofit.create(OpenSubtitleService.class);
    }
}
