package com.magycbytes.opensubtitlelibrary.Network;

import com.magycbytes.opensubtitlelibrary.Models.Network.MethodCall;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by alexandru on 6/25/16.
 */

public interface OpenSubtitleService {

    @POST("/xml-rpc")
    Call<MethodResponse> sendRequest(@Body MethodCall request);

    @POST("/xml-rpc")
    Call<MethodResponse> sendRequest(@Body RequestBody request);
}
