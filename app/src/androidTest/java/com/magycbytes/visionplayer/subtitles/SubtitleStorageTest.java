package com.magycbytes.visionplayer.subtitles;

import android.text.Html;
import android.text.Spanned;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tyranul on 3/12/16.
 */
@SuppressWarnings({"HardCodedStringLiteral", "deprecation"})
public class SubtitleStorageTest extends TestCase {

    private SubtitleStorage subtitleStorage;

    protected void setUp() throws Exception {
        super.setUp();

        List<SubtitleText> subtitleTextList = new ArrayList<>(3);

        SubtitleText firstSubtitleText = new SubtitleText("00:00:05,000", "00:00:07,345");
        firstSubtitleText.setPhraseText("First line");

        SubtitleText secondSubtitleText = new SubtitleText("00:00:10,250", "00:00:13,000");
        secondSubtitleText.setPhraseText("Second line");

        SubtitleText thirdSubtitleText = new SubtitleText("00:00:13,001", "00:00:13,500");
        thirdSubtitleText.setPhraseText("Third line");

        subtitleTextList.add(firstSubtitleText);
        subtitleTextList.add(secondSubtitleText);
        subtitleTextList.add(thirdSubtitleText);

        subtitleStorage = new SubtitleStorage(subtitleTextList);
    }

    public void test_getSubtitleAtIndicatedTime_extractFirstSubtitle_ReturnTextWithSuccess() {
        SubtitleToShow subtitleToShow = subtitleStorage.getSubtitleAtIndicatedTime(5 * 1000 + 10);
        assertNotNull(subtitleToShow);

        Spanned resultText = subtitleToShow.getPhraseTextToShowNow();
        assertEquals(Html.fromHtml("First line"), resultText);
        assertEquals(7345 - 5010, subtitleToShow.getTimeIntervalToShowCurrentSubtitle());
        assertEquals(10250 - 5010, subtitleToShow.getNextTimeToCheckForSubtitle());
    }

    public void test_getSubtitleAtIndicatedTime_requestZeroSubtitle_ReturnEmptySubtitle() {
        SubtitleToShow subtitleToShow = subtitleStorage.getSubtitleAtIndicatedTime(3000);

        assertEquals(Html.fromHtml(""), subtitleToShow.getPhraseTextToShowNow());
        assertEquals(5000 - 3000, subtitleToShow.getNextTimeToCheckForSubtitle());
        assertEquals(-1, subtitleToShow.getTimeIntervalToShowCurrentSubtitle());
    }

    public void test_getSubtitleAtIndicatedTime_requestLastPlusOne_ReturnNull() {
        SubtitleToShow subtitleToShow = subtitleStorage.getSubtitleAtIndicatedTime(360000);

        assertNull(subtitleToShow);
    }

    public void test_getSubtitleAtIndicatedTime_requestLast_ReturnThirdSubtitle() {
        SubtitleToShow subtitleToShow = subtitleStorage.getSubtitleAtIndicatedTime(13001);

        assertEquals(Html.fromHtml("Third line"), subtitleToShow.getPhraseTextToShowNow());
        assertEquals(-1, subtitleToShow.getNextTimeToCheckForSubtitle());
        assertEquals(499, subtitleToShow.getTimeIntervalToShowCurrentSubtitle());
    }

}