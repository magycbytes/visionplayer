package com.magycbytes.visionplayer.utilities;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by magyc Bytes on 16.02.2016.
 */
public class TimeUtilities {
    public static long getTimeIntervalAsMilliseconds(String timeToParse) {
        Integer hours = Integer.valueOf(timeToParse.substring(0, 2));
        Integer minutes = Integer.valueOf(timeToParse.substring(3, 5));
        Integer seconds = Integer.valueOf(timeToParse.substring(6, 8));
        Integer milliseconds = Integer.valueOf(timeToParse.substring(timeToParse.indexOf(',') + 1));

        long totalMilliSecondsInOneMinute = 60 * 1000;
        long totalMilliSecondsInOneHour = totalMilliSecondsInOneMinute * 60;

        return hours * totalMilliSecondsInOneHour +
                minutes * totalMilliSecondsInOneMinute +
                seconds * 1000 +
                milliseconds;
    }

    public static String convertToString(long timeInMilliseconds) {
        long hours = TimeUnit.MILLISECONDS.toHours(timeInMilliseconds);
        timeInMilliseconds = timeInMilliseconds - TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds);
        timeInMilliseconds = timeInMilliseconds - TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds);
        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds); //NON-NLS
    }
}
