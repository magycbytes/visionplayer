package com.magycbytes.visionplayer;

import android.content.Context;

/**
 * Created by tyranul on 4/5/16.
 */
public class DrawableIdFinder {
    private static final String RESERVED_WORD_DO = "do"; //NON-NLS
    private static final String FLAG_DO = "flag_do"; //NON-NLS
    private static final String SEARCH_FOLDER_DRAWABLE = "drawable"; //NON-NLS

    public static int extract(String isoLanguage, Context context) {
        if (isoLanguage.equals(RESERVED_WORD_DO)) {
            isoLanguage = FLAG_DO;
        }
        return context.getResources().getIdentifier(isoLanguage, SEARCH_FOLDER_DRAWABLE, context.getPackageName());
    }
}
