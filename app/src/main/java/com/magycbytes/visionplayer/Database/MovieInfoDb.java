package com.magycbytes.visionplayer.Database;

import com.magycbytes.visionplayer.Models.DbSubtitle;

import java.util.List;

/**
 * Created by magyc Bytes on 25.02.2016.
 */
public class MovieInfoDb {
    private final String movieHash;
    private final int lastMoviePosition;
    private final List<DbSubtitle> currentMovieSubtitle;

    public MovieInfoDb(String movieHash, int lastMoviePosition, List<DbSubtitle> currentMovieSubtitle) {
        this.movieHash = movieHash;
        this.lastMoviePosition = lastMoviePosition;
        this.currentMovieSubtitle = currentMovieSubtitle;
    }

    public String getMovieHash() {
        return movieHash;
    }

    public int getLastPosition() {
        return lastMoviePosition;
    }

    public List<DbSubtitle> getCurrentMovieSubtitle() {
        return currentMovieSubtitle;
    }

}
