package com.magycbytes.visionplayer.Database;

import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.visionplayer.MovieInformationExtractor;
import com.magycbytes.visionplayer.VisionPlayerApp;

/**
 * Created by tyranul on 3/8/16.
 */
public class DatabaseFacade {

    public DatabaseFacade() {
    }

    public void saveDownloadedSubtitle(Subtitle subtitle, String targetMovieHash) {
        DatabaseReader databaseReader = new DatabaseReader(VisionPlayerApp.sMoviesSqlHelper.getReadableDatabase());
        int currentVideoFileID = databaseReader.getMovieId(targetMovieHash);

        DatabaseWriter databaseWriter = new DatabaseWriter(VisionPlayerApp.sMoviesSqlHelper.getWritableDatabase());
        databaseWriter.saveMovieSubtitles(subtitle, currentVideoFileID);
    }

    public MovieInfoDb extractMovieInformation(String movieHash) {
        DatabaseReader databaseReader = new DatabaseReader(VisionPlayerApp.sMoviesSqlHelper.getReadableDatabase());

        MovieInformationExtractor movieInformationExtractor = new MovieInformationExtractor(databaseReader, movieHash);
        movieInformationExtractor.extractLastMoviePosition();
        movieInformationExtractor.extractMovieSubtitles();

        return movieInformationExtractor.getExtractedInformation();
    }
}
