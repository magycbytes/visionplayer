package com.magycbytes.visionplayer.Database.movieAccess;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.magycbytes.visionplayer.Database.MovieInfoContract;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.RecentVideo;

/**
 * Created by Alex on 15/09/2016.
 */

class MovieAccessWriter {

    private static final String LOG_TAG = MovieAccessWriter.class.getSimpleName();

    private final SQLiteDatabase mSQLiteDatabase;
    private final RecentVideo mRecentVideo;
    private final String mTableName;
    private final ContentValues mContentValues = new ContentValues();

    MovieAccessWriter(SQLiteDatabase sqLiteDatabase, RecentVideo recentVideo) {
        mSQLiteDatabase = sqLiteDatabase;
        mRecentVideo = recentVideo;

        mTableName = MovieInfoContract.MoviesAccess.TABLE_NAME;
        createContentValues();
    }

    void insert() {
        long insertId = mSQLiteDatabase.insert(MovieInfoContract.MoviesAccess.TABLE_NAME, null, mContentValues);
        if (insertId == -1) showErrorInLog();
    }

    void update() {
        String whereClause = MovieInfoContract.MoviesAccess.MOVIE_PATH + " LIKE ?";
        String[] whereArguments = new String[] {
                mRecentVideo.getFilePath()
        };

        int update = mSQLiteDatabase.update(mTableName, mContentValues, whereClause, whereArguments);
        if (update == -1) showErrorInLog();
    }

    private void createContentValues() {
        mContentValues.put(MovieInfoContract.MoviesAccess.MOVIE_PATH, mRecentVideo.getFilePath());
        mContentValues.put(MovieInfoContract.MoviesAccess.LAST_OPENED_TIME, mRecentVideo.getLastOpenedTime());
        mContentValues.put(MovieInfoContract.MoviesAccess.MOVIE_NAME, mRecentVideo.getFileName());
    }

    private void showErrorInLog() {
        Log.e(LOG_TAG, "Error at inserting/updating time for movie: " + mRecentVideo.getFilePath());
    }
}
