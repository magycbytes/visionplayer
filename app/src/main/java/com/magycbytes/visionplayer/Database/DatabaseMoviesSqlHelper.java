package com.magycbytes.visionplayer.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.magycbytes.visionplayer.Database.SqlStatements.SQL_CREATE_TABLE_ACCESS_TIME;
import static com.magycbytes.visionplayer.Database.SqlStatements.SQL_CREATE_TABLE_MOVIES;
import static com.magycbytes.visionplayer.Database.SqlStatements.SQL_CREATE_TABLE_SUBTITLES;

/**
 * Created by tyranul on 11/4/15.
 */
public class DatabaseMoviesSqlHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "moviesInfo.db"; //NON-NLS

    public DatabaseMoviesSqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(SQL_CREATE_TABLE_MOVIES);
            db.execSQL(SQL_CREATE_TABLE_SUBTITLES);
            db.execSQL(SQL_CREATE_TABLE_ACCESS_TIME);
        } catch (SQLException exception) {
            Log.e("SQL", "Error at creating database: " + exception.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            db.execSQL(SQL_CREATE_TABLE_ACCESS_TIME);
        }
    }
}

