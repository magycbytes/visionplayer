package com.magycbytes.visionplayer.Database;


import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.google.firebase.crash.FirebaseCrash;
import com.magycbytes.visionplayer.Models.DbSubtitle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by magyc Bytes on 11/5/15.
 */
public class DatabaseReader {

    private final SQLiteDatabase databaseReader;

    DatabaseReader(SQLiteDatabase databaseReader) {
        this.databaseReader = databaseReader;
    }

    public int getLastMoviePosition(String hashFilm) {
        if (hashFilm == null) {
            FirebaseCrash.log("Trying to extract movie position for a null hash");
            return -1;
        }

        String[] projecting = {MovieInfoContract.MovieInfo.LAST_POSITION};
        String filters = MovieInfoContract.MovieInfo.MOVIE_HASH + "=?";
        String[] filterArguments = {hashFilm};

        Cursor cursor = databaseReader.query(MovieInfoContract.MovieInfo.MOVIE_TABLE, projecting, filters, filterArguments, null, null, null);

        int result = -1;
        if (cursor.moveToFirst()) {
            result = cursor.getInt(cursor.getColumnIndex(projecting[0]));
        }
        cursor.close();
        return result;
    }

    public int getMovieId(String movieHash) {
        if (movieHash == null) return -1;

        String[] projecting = {MovieInfoContract.MovieInfo.MOVIE_ID};
        String filters = MovieInfoContract.MovieInfo.MOVIE_HASH + "=?";
        String[] filterArguments = {movieHash};

        Cursor cursor = databaseReader.query(MovieInfoContract.MovieInfo.MOVIE_TABLE, projecting, filters, filterArguments, null, null, null);

        int result = -1;
        if (cursor.moveToFirst()) {
            result = cursor.getInt(cursor.getColumnIndex(projecting[0]));
        }
        cursor.close();
        return result;
    }

    public ArrayList<DbSubtitle> getMovieExistingSubtitles(String movieId) throws SQLException {
        String[] projecting = {
                MovieInfoContract.MovieSubtitles.SUBTITLE_FILE_NAME,
                MovieInfoContract.MovieSubtitles.SUBTITLE_SERVER_ID,
                MovieInfoContract.MovieSubtitles.SUBTITLE_LANGUAGE_ISO,
                MovieInfoContract.MovieSubtitles.SUBTITLE_ENCODING};
        String filters = MovieInfoContract.MovieInfo.MOVIE_ID + "=?";
        String[] filtersArguments = {movieId};

        Cursor cursor = databaseReader.query(MovieInfoContract.MovieSubtitles.SUBTITLES_TABLE, projecting, filters, filtersArguments, null, null, null);
        ArrayList<DbSubtitle> results = new ArrayList<>();
        if (cursor.moveToFirst()) {
            extractAndAppendMovieSubtitleFromCursor(cursor, projecting, results);
            while (cursor.moveToNext()) {
                extractAndAppendMovieSubtitleFromCursor(cursor, projecting, results);
            }
        }
        cursor.close();
        return results;
    }

    private void extractAndAppendMovieSubtitleFromCursor(Cursor cursor, String[] projecting, List<DbSubtitle> listWithMovieSubtitles) {
        String subtitleServerID = cursor.getString(cursor.getColumnIndex(projecting[1]));
        String subtitleLanguageISO639 = cursor.getString(cursor.getColumnIndex(projecting[2]));
        String subtitleFileName = cursor.getString(cursor.getColumnIndex(projecting[0]));
        String subtitleSavedEncoding = cursor.getString(cursor.getColumnIndex(projecting[3]));

        DbSubtitle dbSubtitle = new DbSubtitle(subtitleServerID, subtitleLanguageISO639, subtitleSavedEncoding, subtitleFileName);
        listWithMovieSubtitles.add(dbSubtitle);
    }
}
