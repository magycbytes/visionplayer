package com.magycbytes.visionplayer.Database.movieAccess;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.magycbytes.visionplayer.Database.MovieInfoContract;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.RecentVideo;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 14/09/2016.
 */

public class MovieAccessCrud {

    public static void save(RecentVideo recentVideo, SQLiteDatabase sqLiteDatabase) {
        MovieAccessWriter writer = new MovieAccessWriter(sqLiteDatabase, recentVideo);

        if (isVideoRegistered(recentVideo, sqLiteDatabase)) writer.update();
        else writer.insert();
    }

    public static List<VideoFileInfo> getAllRecentVideos(SQLiteDatabase sqLiteDatabase) {
        String[] columns = new String[] {
                MovieInfoContract.MoviesAccess.MOVIE_PATH,
                MovieInfoContract.MoviesAccess.MOVIE_NAME
        };

        String order = MovieInfoContract.MoviesAccess.LAST_OPENED_TIME + " DESC";

        Cursor cursor = sqLiteDatabase.query(MovieInfoContract.MoviesAccess.TABLE_NAME, columns, null, null, null, null, order);

        List<VideoFileInfo> allRecentVideos = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                allRecentVideos.add(extractRecentVideoFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return allRecentVideos;
    }

    private static VideoFileInfo extractRecentVideoFromCursor(Cursor cursor) {
        String moviePath = cursor.getString(cursor.getColumnIndex(MovieInfoContract.MoviesAccess.MOVIE_PATH));
        String movieName = cursor.getString(cursor.getColumnIndex(MovieInfoContract.MoviesAccess.MOVIE_NAME));

        return new VideoFileInfo(moviePath, movieName);
    }

    private static boolean isVideoRegistered(RecentVideo recentVideo, SQLiteDatabase sqLiteDatabase) {
        String selection = MovieInfoContract.MoviesAccess.MOVIE_PATH + " LIKE ?";
        String[] selectionArgs = new String[] {
                recentVideo.getFilePath()
        };

        Cursor cursor = sqLiteDatabase.query(MovieInfoContract.MoviesAccess.TABLE_NAME, new String[] {MovieInfoContract.MoviesAccess.LAST_OPENED_TIME}, selection, selectionArgs, null, null, null);
        boolean isVideoRegistered = cursor.getCount() > 0;
        cursor.close();
        return isVideoRegistered;
    }
}
