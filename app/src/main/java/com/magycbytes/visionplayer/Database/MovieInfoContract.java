package com.magycbytes.visionplayer.Database;

/**
 * Created by tyranul on 3/30/16.
 */
public class MovieInfoContract {

    abstract class MovieInfo {
        static final String MOVIE_TABLE = "Movies"; 
        static final String MOVIE_ID = "movieId"; 
        static final String MOVIE_HASH = "movieHash"; 
        static final String LAST_POSITION = "lastPosition"; 
    }

    abstract class MovieSubtitles {
        static final String SUBTITLES_TABLE = "Subtitles"; 
        static final String SUBTITLE_ID = "subtitleId"; 
        static final String SUBTITLE_SERVER_ID = "subtitleServerId"; 
        static final String SUBTITLE_LANGUAGE_ISO = "subtitleLanguageISO"; 
        static final String SUBTITLE_ENCODING = "subtitleEncoding"; 
        static final String SUBTITLE_FILE_NAME = "subtitleFileName"; 
    }

    public abstract class MoviesAccess {
        public static final String TABLE_NAME = "MovieAccess";
        static final String ACCESS_ID = "AccessId";
        public static final String LAST_OPENED_TIME = "LastOpenedTime";
        public static final String MOVIE_PATH = "MoviePath";
        public static final String MOVIE_NAME = "MovieName";
    }
}
