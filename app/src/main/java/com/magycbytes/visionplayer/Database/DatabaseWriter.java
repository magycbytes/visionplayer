package com.magycbytes.visionplayer.Database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;


/**
 * Created by magyc Bytes on 11/5/15.
 */
public class DatabaseWriter {
    private final SQLiteDatabase databaseWriter;

    public DatabaseWriter(SQLiteDatabase databaseWriter) {
        this.databaseWriter = databaseWriter;
    }


    public void saveMoviePosition(String movieHash, int currentPosition) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MovieInfoContract.MovieInfo.MOVIE_HASH, movieHash);
        contentValues.put(MovieInfoContract.MovieInfo.LAST_POSITION, currentPosition);

        long rowUpdated = databaseWriter.update(
                MovieInfoContract.MovieInfo.MOVIE_TABLE,
                contentValues,
                MovieInfoContract.MovieInfo.MOVIE_HASH + "= ?",
                new String[]{movieHash});
        if (rowUpdated == 0) {
            databaseWriter.insert(MovieInfoContract.MovieInfo.MOVIE_TABLE, null, contentValues);
        }
    }

    public void saveMovieSubtitles(Subtitle subtitle, int movieId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MovieInfoContract.MovieInfo.MOVIE_ID, movieId);
        contentValues.put(MovieInfoContract.MovieSubtitles.SUBTITLE_FILE_NAME, subtitle.getSubtitleFileName());
        contentValues.put(MovieInfoContract.MovieSubtitles.SUBTITLE_SERVER_ID, subtitle.getIdSubtitleFile());
        contentValues.put(MovieInfoContract.MovieSubtitles.SUBTITLE_LANGUAGE_ISO, subtitle.getLanguageIso639());
        contentValues.put(MovieInfoContract.MovieSubtitles.SUBTITLE_ENCODING, subtitle.getSubtitleEncoding());
        databaseWriter.insert(MovieInfoContract.MovieSubtitles.SUBTITLES_TABLE, null, contentValues);
    }

}

