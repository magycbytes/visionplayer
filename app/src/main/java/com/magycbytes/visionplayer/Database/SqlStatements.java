package com.magycbytes.visionplayer.Database;

/**
 * Created by Alex on 14/09/2016.
 */

class SqlStatements {

    static final String SQL_CREATE_TABLE_MOVIES =
            "CREATE TABLE IF NOT EXISTS " + MovieInfoContract.MovieInfo.MOVIE_TABLE + " (" +
                    MovieInfoContract.MovieInfo.MOVIE_ID + " INTEGER PRIMARY KEY, " +
                    MovieInfoContract.MovieInfo.MOVIE_HASH + " VARCHAR(255) NOT NULL, " +
                    MovieInfoContract.MovieInfo.LAST_POSITION + " INTEGER);";

    static final String SQL_CREATE_TABLE_SUBTITLES =
            "CREATE TABLE IF NOT EXISTS " + MovieInfoContract.MovieSubtitles.SUBTITLES_TABLE + " (" +
                    MovieInfoContract.MovieSubtitles.SUBTITLE_ID + " INTEGER PRIMARY KEY, " +
                    MovieInfoContract.MovieInfo.MOVIE_ID + " INTEGER NOT NULL, " +
                    MovieInfoContract.MovieSubtitles.SUBTITLE_SERVER_ID + " VARCHAR(32) NOT NULL, " +
                    MovieInfoContract.MovieSubtitles.SUBTITLE_LANGUAGE_ISO + " CHAR(2) NOT NULL, " +
                    MovieInfoContract.MovieSubtitles.SUBTITLE_ENCODING + " VARCHAR(32) NOT NULL, " +
                    MovieInfoContract.MovieSubtitles.SUBTITLE_FILE_NAME + " VARCHAR(512) NOT NULL);";

    static final String SQL_CREATE_TABLE_ACCESS_TIME =
            "CREATE TABLE IF NOT EXISTS " + MovieInfoContract.MoviesAccess.TABLE_NAME + " ( " +
                    MovieInfoContract.MoviesAccess.ACCESS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MovieInfoContract.MoviesAccess.MOVIE_PATH + " VARCHAR(512) NOT NULL, " +
                    MovieInfoContract.MoviesAccess.MOVIE_NAME + " VARCHAR(256) NOT NULL, " +
                    MovieInfoContract.MoviesAccess.LAST_OPENED_TIME + " INTEGER);";
}
