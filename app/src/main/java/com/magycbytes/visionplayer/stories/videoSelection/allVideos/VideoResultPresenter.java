package com.magycbytes.visionplayer.stories.videoSelection.allVideos;

import android.content.Context;

import com.magycbytes.visionplayer.screens.videoSelecting.service.SaveMovieOpeningTimeService;
import com.magycbytes.visionplayer.stories.videoPlayback.VideoPlaybackActivity;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideosView;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.BaseRecyclerAdapter;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.VideoListEvents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 15/09/2016.
 */

class VideoResultPresenter implements VideoListEvents {

    private final BaseRecyclerAdapter mAdapter;
    private final Context mContext;

    VideoResultPresenter(BaseVideosView view, Context context) {
        mContext = context;
        mAdapter = new BaseRecyclerAdapter(new ArrayList<VideoFileInfo>(), this);
        view.setAdapter(mAdapter);
    }

    void showFoundItems(List<VideoFileInfo> videoList) {
        mAdapter.addAllItems(videoList);
    }

    @Override
    public void onVideoSelectedForPlay(VideoFileInfo videoInfo) {
        SaveMovieOpeningTimeService.save(mContext, videoInfo);
        VideoPlaybackActivity.start(mContext, videoInfo.getFilePath());
    }
}
