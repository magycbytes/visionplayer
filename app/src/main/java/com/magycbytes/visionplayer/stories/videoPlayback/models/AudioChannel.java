package com.magycbytes.visionplayer.stories.videoPlayback.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tyranul on 3/28/16.
 */
public class AudioChannel implements Parcelable {
    private final String mCanalLanguage;
    private final int mTrackIndex;

    public AudioChannel(String languageCanal, int trackIndex) {
        this.mCanalLanguage = languageCanal;
        this.mTrackIndex = trackIndex;
    }

    private AudioChannel(Parcel in) {
        mCanalLanguage = in.readString();
        mTrackIndex = in.readInt();
    }

    public static final Creator<AudioChannel> CREATOR = new Creator<AudioChannel>() {
        @Override
        public AudioChannel createFromParcel(Parcel in) {
            return new AudioChannel(in);
        }

        @Override
        public AudioChannel[] newArray(int size) {
            return new AudioChannel[size];
        }
    };

    public String getCanalLanguage() {
        return mCanalLanguage;
    }

    public int getTrackIndex() {
        return mTrackIndex;
    }

    @Override
    public String toString() {
        return mCanalLanguage + " with track index: " + mTrackIndex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mCanalLanguage);
        parcel.writeInt(mTrackIndex);
    }
}
