package com.magycbytes.visionplayer.stories.videoPlayback.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.magycbytes.visionplayer.Models.DbSubtitle;
import com.magycbytes.visionplayer.R;

import java.util.ArrayList;

/**
 * Created by tyranul on 8/14/16.
 */

public class SubtitlesDialog extends DialogFragment implements AdapterView.OnItemClickListener {

    private static final String SUBTITLES = "subtitles";
    private static final String CURRENT_SUBTITLE = "currentSubtitle";
    private SubtitleToSelectAdapter subtitleToSelectAdapter;
    private SubtitleDialogEvents mEventsListener;

    public static DialogFragment create(ArrayList<DbSubtitle> subtitles, DbSubtitle currentSubtitle, SubtitleDialogEvents eventsListener) {
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList(SUBTITLES, subtitles);
        arguments.putParcelable(CURRENT_SUBTITLE, currentSubtitle);

        SubtitlesDialog subtitlesDialog = new SubtitlesDialog();
        subtitlesDialog.setArguments(arguments);
        subtitlesDialog.setEventsListener(eventsListener);

        return subtitlesDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View view = LayoutInflater.from(getContext()).inflate(R.layout.subtitle_to_select, null);

        subtitleToSelectAdapter = new SubtitleToSelectAdapter(getArguments().<DbSubtitle>getParcelableArrayList(SUBTITLES), getActivity());
        subtitleToSelectAdapter.setCurrentSelectedSubtitle((DbSubtitle) getArguments().getParcelable(CURRENT_SUBTITLE));

        ListView listView = (ListView) view.findViewById(R.id.listWithSubtitleToSelect);
        listView.setOnItemClickListener(this);
        listView.setAdapter(subtitleToSelectAdapter);

        builder.setTitle(R.string.title_select_subtitle);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        dismiss();
        subtitleToSelectAdapter.setCurrentSelectedSubtitle(i);
        mEventsListener.onSubtitleSelected(subtitleToSelectAdapter.getSubtitle(i));
    }

    private void setEventsListener(SubtitleDialogEvents eventsListener) {
        mEventsListener = eventsListener;
    }

    public interface SubtitleDialogEvents {
        void onSubtitleSelected(DbSubtitle subtitle);
    }
}
