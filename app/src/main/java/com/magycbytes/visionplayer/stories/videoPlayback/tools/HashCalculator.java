package com.magycbytes.visionplayer.stories.videoPlayback.tools;


import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.LongBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by tyranul on 11/4/15.
 * Algorithm is taken from Opensubtitles.org site
 */
public class HashCalculator {
    /**
     * Size of the chunks that will be hashed in bytes (64 KB)
     */
    private static final int HASH_CHUNK_SIZE = 64 * 1024;
    private static final String LOG_TAG = HashCalculator.class.getSimpleName();


    public static String computeHash(String filePath) throws IOException {
        File file = new File(filePath);

        long size = file.length();
        long chunkSizeForFile = Math.min(HASH_CHUNK_SIZE, size);

        FileChannel fileChannel = new FileInputStream(file).getChannel();

        try {
            long head = computeHashForChunk(fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, chunkSizeForFile));
            long tail = computeHashForChunk(fileChannel.map(FileChannel.MapMode.READ_ONLY, Math.max(size - HASH_CHUNK_SIZE, 0), chunkSizeForFile));

            return String.format("%016x", size + head + tail); //NON-NLS
        } catch (IllegalArgumentException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            return null;
        } finally {
            closeChannel(fileChannel);
        }
    }


    private static long computeHashForChunk(ByteBuffer buffer) {

        LongBuffer longBuffer = buffer.order(ByteOrder.LITTLE_ENDIAN).asLongBuffer();
        long hash = 0;

        while (longBuffer.hasRemaining()) {
            hash += longBuffer.get();
        }

        return hash;
    }

    private static void closeChannel(FileChannel fileChannel) {
        try {
            fileChannel.close();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error at closing channel: " + e.getMessage(), e);
        }
    }

}
