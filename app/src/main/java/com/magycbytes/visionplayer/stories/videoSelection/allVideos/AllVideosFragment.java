package com.magycbytes.visionplayer.stories.videoSelection.allVideos;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideoCollectionFragment;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideosView;


public class AllVideosFragment extends BaseVideoCollectionFragment {

    private AllVideosPresenter mPresenter;

    public static Fragment newInstance() {
        return new AllVideosFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_videos_listing, container, false);
        BaseVideosView baseVideosView = new BaseVideosView(view);
        mPresenter = new AllVideosPresenter(baseVideosView, getLoaderManager(), getContext());

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mPresenter.loadFromDatabase();
        }

        return view;
    }

    @Override
    public void loadFromDatabase() {
        if (isAdded() && mPresenter != null) mPresenter.loadFromDatabase();
    }

    @Override
    public void resetLayout() {
        if (isAdded() && mPresenter != null) mPresenter.resetCurrentLayout();
    }
}
