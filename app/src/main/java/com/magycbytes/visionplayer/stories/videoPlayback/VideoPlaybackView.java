package com.magycbytes.visionplayer.stories.videoPlayback;

import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoPlayback.customViews.PlaybackControlPanelView;
import com.magycbytes.visionplayer.stories.videoPlayback.customViews.VideoViewEx;
import com.magycbytes.visionplayer.stories.videoPlayback.models.AudioChannel;

import java.util.ArrayList;

/**
 * Created by tyranul on 8/7/16.
 */

class VideoPlaybackView {

    private final PlaybackControlPanelView mPanelView;
    private final VideoViewEx mVideoView;

    VideoPlaybackView(View view) {
        mVideoView = (VideoViewEx) view.findViewById(R.id.video_surface);
        mPanelView = (PlaybackControlPanelView) view.findViewById(R.id.control_panel);
    }

    void playVideoFile(Uri movie) {
        mVideoView.setVideoURI(movie);
        mVideoView.start();
    }

    void changePlayState() {
        if (mVideoView.isPlaying()) {
            mVideoView.pause();
        } else {
            mVideoView.start();
        }
    }

    void setVolume(float volume) {
        mVideoView.setVolumeLevel(volume);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    int getCurrentAudioTrack() {
        if (mVideoView == null) return -1;
        return mVideoView.getCurrentAudioTrack();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    void setAudioTrack(int indexAudioTrack) {
        if (indexAudioTrack < 0) {
            return;
        }
        mVideoView.setAudioTrack(indexAudioTrack);
    }

    ArrayList<AudioChannel> getAudioChannels() {
        return mVideoView.getMovieAudioChannels();
    }

    void showVideoDuration(int durationMilliseconds) {
        mPanelView.showVideoDuration(durationMilliseconds);
    }

    void showCurrentVideoPosition() {
        int currentPosition = mVideoView.getCurrentPosition();
        mPanelView.showVideoPosition(currentPosition);
    }

    void seekVideoTo(int videoPosition) {
        mVideoView.seekTo(videoPosition);
    }

    void moveForwardWith(int milliseconds) {
        if (!mVideoView.isPlaying()) return;

        int difference = mVideoView.getCurrentPosition() + milliseconds;
        if (difference > mVideoView.getDuration()) difference = mVideoView.getDuration() - 1000;

        seekVideoTo(difference);
    }

    void moveBackWith(int milliseconds) {
        if (!mVideoView.isPlaying()) return;

        int difference = mVideoView.getCurrentPosition() - milliseconds;
        if (difference < 1) difference = 0;

        seekVideoTo(difference);
    }

    int getVideoPosition() {
        return mVideoView.getCurrentPosition();
    }

    void setVideoEvents(VideoViewEx.VideoViewEvents eventsListener) {
        mVideoView.setEventsListener(eventsListener);
    }

    void setPanelEventsListener(PlaybackControlPanelView.PlaybackControlPanelEvents eventsListener) {
        mPanelView.setEventsListener(eventsListener);
    }

    boolean isVideoPlaying() {
        return mVideoView.isPlaying();
    }
}
