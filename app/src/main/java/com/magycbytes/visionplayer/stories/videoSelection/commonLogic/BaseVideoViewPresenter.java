package com.magycbytes.visionplayer.stories.videoSelection.commonLogic;

import android.content.Context;
import android.support.v4.app.LoaderManager;

import com.magycbytes.visionplayer.screens.videoSelecting.service.SaveMovieOpeningTimeService;
import com.magycbytes.visionplayer.stories.videoPlayback.VideoPlaybackActivity;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.IBaseRecyclerAdapter;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.VideoListEvents;

import java.util.ArrayList;

/**
 * Created by Alex on 15/09/2016.
 */

public abstract class BaseVideoViewPresenter implements VideoListEvents, BaseVideosView.BaseVideoViewEvents {

    protected final BaseVideosView mView;
    protected final LoaderManager mLoaderManager;
    protected final Context mContext;
    protected IBaseRecyclerAdapter mAdapter;

    protected BaseVideoViewPresenter(BaseVideosView view, LoaderManager loaderManager, Context context) {
        mView = view;
        mLoaderManager = loaderManager;
        mContext = context;

        mView.setEventsListener(this);

        setEmptyAdapter();
    }

    @Override
    public void onVideoSelectedForPlay(VideoFileInfo videoInfo) {
        SaveMovieOpeningTimeService.save(mContext, videoInfo);
        VideoPlaybackActivity.start(mContext, videoInfo.getFilePath());
    }

    public void resetCurrentLayout() {
        setEmptyAdapter();
        loadFromDatabase();
    }

    private void setEmptyAdapter() {
        mAdapter = VideoAdapterFactory.setCurrentAdapter(mView, new ArrayList<VideoFileInfo>(), mContext, this);
    }

    protected abstract void loadFromDatabase();

    @Override
    public void onRetryDatabase() {
        loadFromDatabase();
    }
}
