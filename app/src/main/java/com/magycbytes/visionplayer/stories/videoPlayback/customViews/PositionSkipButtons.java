package com.magycbytes.visionplayer.stories.videoPlayback.customViews;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.magycbytes.visionplayer.R;

/**
 * Created by Alex on 24/10/2016.
 */

class PositionSkipButtons implements View.OnClickListener {

    private static final String LOG_TAG = PositionSkipButtons.class.getSimpleName();

    private PositionSkipButtonsEvents mEventsListener;

    PositionSkipButtons(View view) {
        ImageButton previous = (ImageButton) view.findViewById(R.id.previous_button);
        ImageButton next = (ImageButton) view.findViewById(R.id.next_button);

        previous.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (mEventsListener == null) {
            Log.e(LOG_TAG, "Event listener is null");
            return;
        }

        if (view.getId() == R.id.previous_button) mEventsListener.onSkipPrevious();
        else if (view.getId() == R.id.next_button) mEventsListener.onSkipForward();
    }

    public void setEventsListener(PositionSkipButtonsEvents eventsListener) {
        mEventsListener = eventsListener;
    }

    interface PositionSkipButtonsEvents {
        void onSkipForward();

        void onSkipPrevious();
    }
}
