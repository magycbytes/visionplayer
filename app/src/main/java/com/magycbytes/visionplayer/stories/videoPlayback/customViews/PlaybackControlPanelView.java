package com.magycbytes.visionplayer.stories.videoPlayback.customViews;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.utilities.TimeUtilities;

import java.util.concurrent.TimeUnit;


public class PlaybackControlPanelView extends FrameLayout implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private static final String LOG_TAG = PlaybackControlPanelView.class.getSimpleName();

    private TextView mCurrentPosition;
    private TextView mMovieDuration;
    private SeekBar mPositionChanger;
    private SeekBar mVolumeChanger;
    private ImageButton mPlayButton;

    private boolean mPanelIsVisible = false;
    private final Handler mVideoPositionHandler = new Handler();
    private PlaybackControlPanelEvents mEventsListener;
    private final Runnable mVideoPositionRequester = new Runnable() {
        @Override
        public void run() {
            Log.d(this.toString(), "Requesting video position");
            mEventsListener.onGetVideoPosition();
            mVideoPositionHandler.postDelayed(this, TimeUnit.SECONDS.toMillis(1));
        }
    };

    private final Handler mVisibilityHandler = new Handler();
    private final Runnable mVisibilityToggle = new Runnable() {
        @Override
        public void run() {
            mPanelIsVisible = !mPanelIsVisible;
            hide();
        }
    };
    private PositionSkipButtons mPositionSkipButtons;

    public PlaybackControlPanelView(Context context) {
        super(context);
        initLayout(context);
    }

    public PlaybackControlPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public PlaybackControlPanelView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout(context);
    }

    public void showVideoDuration(int durationMilliseconds) {
        mPositionChanger.setMax(durationMilliseconds);

        String time = TimeUtilities.convertToString(durationMilliseconds);
        mMovieDuration.setText(time);
    }

    public void showVideoPosition(int videoPositionMilliseconds) {
        mPositionChanger.setProgress(videoPositionMilliseconds);

        String videoPosition = TimeUtilities.convertToString(videoPositionMilliseconds);
        mCurrentPosition.setText(videoPosition);
    }

    public void setVolumeValue(int volumeValue) {
        Log.d(LOG_TAG, "Current volume in volume changer: " + mVolumeChanger.getProgress() + " with max: " + mVolumeChanger.getMax());
        Log.d(LOG_TAG, "Setting new volume value to: " + volumeValue);
        mVolumeChanger.setProgress(volumeValue);
    }

    public void showPlayState() {
        mPlayButton.setImageResource(R.drawable.pause);
    }

    public void showPauseState() {
        mPlayButton.setImageResource(R.drawable.play);
    }

    public void toggleVisibility() {
        mPanelIsVisible = !mPanelIsVisible;

        if (mPanelIsVisible) {
            show();
            resetHidePanelTimer();
        } else {
            mVisibilityHandler.removeCallbacks(mVisibilityToggle);
            hide();
        }
    }

    public void resetHidePanelTimer() {
        mVisibilityHandler.removeCallbacks(mVisibilityToggle);
        mVisibilityHandler.postDelayed(mVisibilityToggle, TimeUnit.SECONDS.toMillis(3));
    }

    private void show() {
        setVisibility(VISIBLE);
        mEventsListener.onGetVideoPosition();
        mVideoPositionHandler.postDelayed(mVideoPositionRequester, 300);
    }

    private void hide() {
        setVisibility(GONE);
        mVideoPositionHandler.removeCallbacks(mVideoPositionRequester);
    }

    private void initLayout(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_control_panel_playback, this);

        mCurrentPosition = (TextView) view.findViewById(R.id.video_current_position);
        mMovieDuration = (TextView) view.findViewById(R.id.movie_duration);
        mPositionChanger = (SeekBar) view.findViewById(R.id.video_position_changer);
        mVolumeChanger = (SeekBar) view.findViewById(R.id.volume_changer);
        mPlayButton = (ImageButton) view.findViewById(R.id.play_button);

        mPositionSkipButtons = new PositionSkipButtons(view);

        mPlayButton.setOnClickListener(this);
        mPositionChanger.setOnSeekBarChangeListener(this);
        mVolumeChanger.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.play_button) {
            if (mEventsListener == null) {
                Log.w(this.toString(), "No event listener connected");
                return;
            }
            mEventsListener.onPlayButtonClicked();
        }
    }

    public void setEventsListener(PlaybackControlPanelEvents eventsListener) {
        mEventsListener = eventsListener;
        mPositionSkipButtons.setEventsListener(mEventsListener);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (!b) return;

        switch (seekBar.getId()) {
            case R.id.volume_changer:
                float volume = mVolumeChanger.getProgress();
                mEventsListener.onChangeVolume(volume);
                break;
            case R.id.video_position_changer:
                mEventsListener.onSeekTo(i);
                showVideoPosition(i);
                break;
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mVisibilityHandler.removeCallbacks(mVisibilityToggle);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        resetHidePanelTimer();
    }

    public void configureVolume(int maxVolume, int currentVolume) {
        mVolumeChanger.setMax(maxVolume);
        mVolumeChanger.setProgress(currentVolume);
    }

    public interface PlaybackControlPanelEvents extends PositionSkipButtons.PositionSkipButtonsEvents {
        void onPlayButtonClicked();

        void onGetVideoPosition();

        void onSeekTo(int videoPosition);

        void onChangeVolume(float volume);
    }
}
