package com.magycbytes.visionplayer.stories.videoSelection.commonLogic;

/**
 * Created by Alex on 15/09/2016.
 */

public class RecentVideo extends VideoFileInfo {

    private final long mLastOpenedTime;

    public RecentVideo(long lastOpenedTime, String moviePath, String movieName) {
        super(moviePath, movieName);

        mLastOpenedTime = lastOpenedTime;
    }

    public long getLastOpenedTime() {
        return mLastOpenedTime;
    }

}
