package com.magycbytes.visionplayer.stories.videoSelection.allVideos;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideoViewPresenter;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideosView;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 15/09/2016.
 */

class AllVideosPresenter extends BaseVideoViewPresenter implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String STORAGE_EXT_SD_CARD_DCIM = "/storage/extSdCard/DCIM/";


    AllVideosPresenter(BaseVideosView view, LoaderManager loaderManager, Context context) {
        super(view, loaderManager, context);
    }

    @Override
    public void loadFromDatabase() {
        mLoaderManager.restartLoader(1, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        mView.hideEmptyList();
        mView.showProgress();

        String[] mediaColumns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media.TITLE};
        return new CursorLoader(mContext,
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                mediaColumns, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (loader.getId() != 1 || cursor == null || cursor.isClosed()) return;

        List<VideoFileInfo> videoFileInfoList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));

                if (filePath.contains(STORAGE_EXT_SD_CARD_DCIM)) {
                    continue;
                }

                String fileTitle = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));

                videoFileInfoList.add(new VideoFileInfo(filePath, fileTitle));
            } while (!cursor.isClosed() && cursor.moveToNext());
        }
        mView.hideProgress();

        if (videoFileInfoList.isEmpty()) mView.showEmptyList();
        else mView.hideEmptyList();

        mAdapter.addAllItems(videoFileInfoList);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
