package com.magycbytes.visionplayer.stories.videoPlayback.subtitles;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoPlayback.customViews.VideoViewEx;
import com.magycbytes.visionplayer.subtitles.SubtitleToShow;

import static com.magycbytes.visionplayer.R.id.subtitleTextView;

/**
 * Created by tyranul on 8/14/16.
 */

public class SubtitleView {

    private static final String LOG_TAG = SubtitleView.class.getSimpleName();

    private SubtitleViewEvents mEventsListener;
    private final TextView mSubtitleText;
    private final VideoViewEx mVideoViewEx;
    private final Handler subtitleHidingHandler = new Handler();
    private final Handler subtitleHandler = new Handler();
    private final Runnable subtitleHidingRunnable = new Runnable() {
        @Override
        public void run() {
            mSubtitleText.setText("");
        }
    };
    private final Runnable subtitleChanger = new Runnable() {
        @Override
        public void run() {
            mEventsListener.onShowNextSubtitle();
        }
    };

    public SubtitleView(View parentView) {
        mSubtitleText = (TextView) parentView.findViewById(subtitleTextView);
        mVideoViewEx = (VideoViewEx) parentView.findViewById(R.id.video_surface);
    }

    void showSubtitle(SubtitleToShow subtitleToShow) {
        if (!mVideoViewEx.isPlaying()) return;
        if (subtitleToShow == null) {
            Log.w(LOG_TAG, "Trying to play a null subtitle");
            return;
        }
        if (subtitleToShow.getTimeIntervalToShowCurrentSubtitle() < 0) {
            mSubtitleText.setText("");
            subtitleHandler.postDelayed(subtitleChanger, subtitleToShow.getNextTimeToCheckForSubtitle());
        } else {
            mSubtitleText.setText(subtitleToShow.getPhraseTextToShowNow());
            if (subtitleToShow.getNextTimeToCheckForSubtitle() > 0) {
                subtitleHandler.postDelayed(subtitleChanger, subtitleToShow.getNextTimeToCheckForSubtitle());
            }
            subtitleHidingHandler.postDelayed(subtitleHidingRunnable, subtitleToShow.getTimeIntervalToShowCurrentSubtitle());
        }
    }

    int getCurrentVideoPosition() {
        return mVideoViewEx.getCurrentPosition();
    }

    void stopChangers() {
        subtitleHidingHandler.removeCallbacks(subtitleHidingRunnable);
        subtitleHandler.removeCallbacks(subtitleChanger);
    }

    public void setEventsListener(SubtitleViewEvents eventsListener) {
        mEventsListener = eventsListener;
    }

    public interface SubtitleViewEvents {
        void onShowNextSubtitle();
    }
}
