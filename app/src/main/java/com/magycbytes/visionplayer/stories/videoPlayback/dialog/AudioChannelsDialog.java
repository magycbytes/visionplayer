package com.magycbytes.visionplayer.stories.videoPlayback.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoPlayback.models.AudioChannel;

import java.util.ArrayList;

/**
 * Created by tyranul on 8/14/16.
 */

public class AudioChannelsDialog extends DialogFragment {

    private static final String AUDIO_CHANNELS = "audioChannels";
    private static final String CURRENT_CHANNEL = "currentChannel";
    private AudioChannelsDialogEvents mEventsListener;
    private ArrayList<AudioChannel> mAudioChannels;

    public static AudioChannelsDialog create(ArrayList<AudioChannel> audioChannels, int currentChannel, AudioChannelsDialogEvents eventsListener) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(AUDIO_CHANNELS, audioChannels);
        bundle.putInt(CURRENT_CHANNEL, currentChannel);

        AudioChannelsDialog dialog = new AudioChannelsDialog();
        dialog.setEventsListener(eventsListener);
        dialog.setArguments(bundle);

        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        extractAudioChannelsFromArguments();

        int currentChannel = getArguments().getInt(CURRENT_CHANNEL);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.text_extract_audio));
        builder.setSingleChoiceItems(getAudioChannelsNames(), currentChannel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mEventsListener.onAudioSelected(mAudioChannels.get(which).getTrackIndex());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        return builder.create();
    }

    private CharSequence[] getAudioChannelsNames() {
        if (mAudioChannels == null) {
            return null;
        }

        CharSequence[] audioChannelsNames = new CharSequence[mAudioChannels.size()];
        for (int i = 0; i < mAudioChannels.size(); ++i) {
            audioChannelsNames[i] = mAudioChannels.get(i).getCanalLanguage();
        }
        return audioChannelsNames;
    }

    private void extractAudioChannelsFromArguments() {
        mAudioChannels = getArguments().getParcelableArrayList(AUDIO_CHANNELS);
    }

    private void setEventsListener(AudioChannelsDialogEvents eventsListener) {
        mEventsListener = eventsListener;
    }

    public interface AudioChannelsDialogEvents {
        void onAudioSelected(int index);
    }
}
