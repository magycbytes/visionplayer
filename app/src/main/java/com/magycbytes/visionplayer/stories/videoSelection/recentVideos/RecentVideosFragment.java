package com.magycbytes.visionplayer.stories.videoSelection.recentVideos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideoCollectionFragment;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideosView;


public class RecentVideosFragment extends BaseVideoCollectionFragment {


    private RecentVideosPresenter mPresenter;

    public static Fragment newInstance() {
        return new RecentVideosFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos_listing, container, false);

        BaseVideosView baseVideosView = new BaseVideosView(view);
        mPresenter = new RecentVideosPresenter(baseVideosView, getLoaderManager(), getContext());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadFromDatabase();
    }

    @Override
    public void loadFromDatabase() {
        if (isAdded() && mPresenter != null) mPresenter.loadFromDatabase();
    }

    @Override
    public void resetLayout() {
        if (isAdded() && mPresenter != null) mPresenter.resetCurrentLayout();
    }
}
