package com.magycbytes.visionplayer.stories.videoSelection.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoSelection.allVideos.AllVideosFragment;
import com.magycbytes.visionplayer.stories.videoSelection.recentVideos.RecentVideosFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 14/09/2016.
 */

public class VideoSelectingAdapter extends FragmentPagerAdapter {

    private final List<CharSequence> mTitles = new ArrayList<>();

    public VideoSelectingAdapter(FragmentManager fm, Context context) {
        super(fm);

        mTitles.add(context.getString(R.string.recent));
        mTitles.add(context.getString(R.string.text_all));
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return RecentVideosFragment.newInstance();
            case 1:
                return AllVideosFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }
}
