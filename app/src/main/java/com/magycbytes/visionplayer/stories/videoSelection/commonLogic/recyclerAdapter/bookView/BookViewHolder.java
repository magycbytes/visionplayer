package com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.bookView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.magycbytes.visionplayer.R;

/**
 * Created by tyranul on 9/3/16.
 */
class BookViewHolder extends RecyclerView.ViewHolder {

    final LinearLayout mLinearLayout;

    BookViewHolder(View itemView) {
        super(itemView);

        mLinearLayout = (LinearLayout) itemView.findViewById(R.id.containerThumbnails);
    }
}
