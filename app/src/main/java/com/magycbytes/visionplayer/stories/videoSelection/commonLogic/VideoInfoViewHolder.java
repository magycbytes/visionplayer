package com.magycbytes.visionplayer.stories.videoSelection.commonLogic;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.screens.videoSelecting.GridView.ThumbnailLoader;

/**
 * Created by Alex on 15/09/2016.
 */
public class VideoInfoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final ImageView mThumbnail;
    private final TextView mVideoName;
    private final VideoInfoHolderEvents mEventsListener;

    public VideoInfoViewHolder(View itemView, VideoInfoHolderEvents eventsListener) {
        super(itemView);
        mEventsListener = eventsListener;

        itemView.setOnClickListener(this);

        mThumbnail = (ImageView) itemView.findViewById(R.id.videoThumbnail);
        mVideoName = (TextView) itemView.findViewById(R.id.videoFileName);
    }

    public void show(VideoFileInfo videoFileInfo) {
        ThumbnailLoader loader = new ThumbnailLoader(mThumbnail, videoFileInfo.getFilePath());
        loader.execute();

        mVideoName.setText(videoFileInfo.getFileName());
    }

    @Override
    public void onClick(View v) {
        mEventsListener.onItemClicked(getAdapterPosition());
    }

    public interface VideoInfoHolderEvents {
        void onItemClicked(int itemPosition);
    }
}
