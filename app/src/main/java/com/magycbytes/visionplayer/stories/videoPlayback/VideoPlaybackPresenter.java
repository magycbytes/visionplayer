package com.magycbytes.visionplayer.stories.videoPlayback;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import com.magycbytes.visionplayer.Database.MovieInfoDb;
import com.magycbytes.visionplayer.Models.DbSubtitle;
import com.magycbytes.visionplayer.stories.videoPlayback.asyncTasks.MovieInfoLoader;
import com.magycbytes.visionplayer.stories.videoPlayback.customViews.PlaybackControlPanelView;
import com.magycbytes.visionplayer.stories.videoPlayback.customViews.VideoViewEx;
import com.magycbytes.visionplayer.stories.videoPlayback.customViews.VolumeChangeObserver;
import com.magycbytes.visionplayer.stories.videoPlayback.services.SaveMoviePositionService;
import com.magycbytes.visionplayer.stories.videoPlayback.subtitles.SubtitlePresenter;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by tyranul on 8/7/16.
 */

class VideoPlaybackPresenter implements VideoViewEx.VideoViewEvents, PlaybackControlPanelView.PlaybackControlPanelEvents, LoaderManager.LoaderCallbacks<MovieInfoDb>,VolumeChangeObserver.VolumeChangeObserverEvents {

    private static final String LOG_TAG = VideoPlaybackPresenter.class.getSimpleName();
    private static final String MOVIE_PATH = "moviePath";

    private final PlaybackControlPanelView mPanelView;
    private final VideoPlaybackView mPlaybackView;
    private MovieInfoDb mMovieInfo;
    private final Context mContext;
    private final LoaderManager mLoaderManager;
    private VolumeChangeObserver mVolumeChangeObserver;
    private final SubtitlePresenter mSubtitlePresenter;

    // backup fields
    private String mCurrentSubtitleServerId;

    VideoPlaybackPresenter(PlaybackControlPanelView panelView, VideoPlaybackView playbackView, Context context, LoaderManager loaderManager, SubtitlePresenter subtitlePresenter) {
        mPanelView = panelView;
        mPlaybackView = playbackView;
        mContext = context;
        mLoaderManager = loaderManager;
        mSubtitlePresenter = subtitlePresenter;

        mVolumeChangeObserver = new VolumeChangeObserver(mContext, this);

        mPlaybackView.setVideoEvents(this);
        mPlaybackView.setPanelEventsListener(this);
    }

    void startPlaying(Uri filePath) {
        if (filePath == null) {
            Log.w(LOG_TAG, "Trying to play a null file path");
            return;
        }

        mPlaybackView.playVideoFile(filePath);

        Bundle arguments = new Bundle();
        arguments.putString(MOVIE_PATH, filePath.getPath());
        mLoaderManager.restartLoader(0, arguments, this);

        Log.d(this.toString(), "Start playing movie file");
    }

    void saveMoviePositionInDatabase() {
        if (mMovieInfo == null) {
            Log.w(this.toString(), "Trying to save position of a movie that is null");
            return;
        }
        Log.d(this.toString(), "Saving movie position in database");
        SaveMoviePositionService.start(mContext, mMovieInfo.getMovieHash(), mPlaybackView.getVideoPosition());
    }

    String getMovieHash() {
        return mMovieInfo.getMovieHash();
    }

    List<DbSubtitle> getMovieSubtitles() {
        if (mMovieInfo == null) return null;
        return mMovieInfo.getCurrentMovieSubtitle();
    }

    void onResume() {
        mContext.getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, mVolumeChangeObserver);
    }

    void onPause() {
        mContext.getContentResolver().unregisterContentObserver(mVolumeChangeObserver);
    }

    @Override
    public void onTogglePanelVisibility() {
        mPanelView.toggleVisibility();
    }

    @Override
    public void onVideoDurationAvailable(int durationMilliseconds) {
        mPlaybackView.showVideoDuration(durationMilliseconds);
    }

    @Override
    public void onVideoIsStarted() {
        mPanelView.showPlayState();
        mPanelView.resetHidePanelTimer();
        mSubtitlePresenter.reloadSubtitlesPosition();
    }

    @Override
    public void onVideoIsPaused() {
        mPanelView.showPauseState();
        mPanelView.resetHidePanelTimer();
        mSubtitlePresenter.pauseChangers();
    }

    @Override
    public void onPlayButtonClicked() {
        mPlaybackView.changePlayState();
    }

    @Override
    public void onGetVideoPosition() {
        mPlaybackView.showCurrentVideoPosition();
    }

    @Override
    public void onSeekTo(int videoPosition) {
        Log.d(this.toString(), "Request to new video position: " + videoPosition);
        mPlaybackView.seekVideoTo(videoPosition);
        mSubtitlePresenter.reloadSubtitlesPosition();
    }

    @Override
    public void onChangeVolume(float volume) {
        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) volume, AudioManager.FLAG_SHOW_UI);
    }

    void setCurrentSubtitleServerId(String currentSubtitleServerId) {
        if (currentSubtitleServerId == null || currentSubtitleServerId.isEmpty()) return;

        mCurrentSubtitleServerId = currentSubtitleServerId;
        if (mMovieInfo == null) {
            return;
        }

        int subtitlePosition = 0;
        for (int i = 0; i < mMovieInfo.getCurrentMovieSubtitle().size(); ++i) {
            if (mMovieInfo.getCurrentMovieSubtitle().get(i).getSubtitleServerId().equals(currentSubtitleServerId)) {
                subtitlePosition = i;
                break;
            }
        }
        mSubtitlePresenter.showSubtitleFile(mMovieInfo.getCurrentMovieSubtitle().get(subtitlePosition));
    }

    @Override
    public Loader<MovieInfoDb> onCreateLoader(int id, Bundle args) {
        return new MovieInfoLoader(args.getString(MOVIE_PATH), mContext);
    }

    @Override
    public void onLoadFinished(Loader<MovieInfoDb> loader, MovieInfoDb data) {
        if (data == null) return;

        mMovieInfo = data;

        int lastPosition = data.getLastPosition();
        Log.d(this.toString(), "Finished extracting movie info from database: movie_position = " + lastPosition);

        if (lastPosition > mPlaybackView.getVideoPosition()) {
            mPlaybackView.seekVideoTo(lastPosition);
        }

        setCurrentSubtitleServerId(mCurrentSubtitleServerId);
    }

    @Override
    public void onLoaderReset(Loader<MovieInfoDb> loader) {

    }

    @Override
    public void onSkipForward() {
        mPanelView.resetHidePanelTimer();
        mPlaybackView.moveForwardWith((int) TimeUnit.SECONDS.toMillis(10));
        mPanelView.showVideoPosition(mPlaybackView.getVideoPosition());
    }

    @Override
    public void onSkipPrevious() {
        mPanelView.resetHidePanelTimer();
        mPlaybackView.moveBackWith((int) TimeUnit.SECONDS.toMillis(10));
        mPanelView.showVideoPosition(mPlaybackView.getVideoPosition());
    }

    @Override
    public void onVolumeChangedTo(int volumeValue) {
        mPanelView.setVolumeValue(volumeValue);
    }
}
