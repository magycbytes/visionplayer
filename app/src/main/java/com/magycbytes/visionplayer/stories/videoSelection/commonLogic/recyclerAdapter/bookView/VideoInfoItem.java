package com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.bookView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;
import com.magycbytes.visionplayer.screens.videoSelecting.GridView.ThumbnailLoader;

/**
 * Created by alexandru on 6/11/16.
 */
class VideoInfoItem implements View.OnClickListener {

    private final TextView mFileName;
    private final ImageView mThumbnail;
    private final VideoInfoItemEvents mEventsListener;


    VideoInfoItem(View rootView, VideoInfoItemEvents eventsListener) {
        mEventsListener = eventsListener;

        mFileName = (TextView) rootView.findViewById(R.id.videoFileName);
        mThumbnail = (ImageView) rootView.findViewById(R.id.videoThumbnail);

        mFileName.setOnClickListener(this);
        mThumbnail.setOnClickListener(this);
    }

    void setUpView(VideoFileInfo videoFileInfo, int itemPosition) {
        mFileName.setText(videoFileInfo.getFileName());
        mThumbnail.setTag(itemPosition);

        ThumbnailLoader loader = new ThumbnailLoader(mThumbnail, videoFileInfo.getFilePath());
        loader.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.videoFileName:
                showTitle();
                break;
            case R.id.videoThumbnail:
                mEventsListener.onVideoSelected((Integer) mThumbnail.getTag());
                break;
        }
    }

    private void showTitle() {
        Toast.makeText(mFileName.getContext(), mFileName.getText().toString(), Toast.LENGTH_LONG).show();
    }

    interface VideoInfoItemEvents {
        void onVideoSelected(int videoPosition);
    }
}
