package com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.bookView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.screens.videoSelecting.helpers.ColumnsCalculator;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.IBaseRecyclerAdapter;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.VideoListEvents;

import java.util.List;


/**
 * Created by alexandru on 6/11/16.
 */
public class VideoInfoBookAdapter extends RecyclerView.Adapter<BookViewHolder> implements VideoInfoItem.VideoInfoItemEvents, IBaseRecyclerAdapter {

    private final List<VideoFileInfo> mVideoFiles;
    private final LayoutInflater mInflater;
    private final int mItemsInRow;
    private final VideoListEvents mEventsListener;

    public VideoInfoBookAdapter(List<VideoFileInfo> videoFiles, Context context, VideoListEvents eventsListener) {
        mVideoFiles = videoFiles;

        mInflater = LayoutInflater.from(context);
        mItemsInRow = ColumnsCalculator.getNumberOfColumns(context);
        mEventsListener = eventsListener;
    }

    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item_book_view, parent, false);

        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookViewHolder holder, int position) {
        int startPosition = position * mItemsInRow;
        int elementsToShow = startPosition + mItemsInRow;
        holder.mLinearLayout.removeAllViews();
        for (int i = startPosition; i < elementsToShow && i < mVideoFiles.size(); ++i) {
            View view = mInflater.inflate(R.layout.video_file_info_2, null, false);

            VideoInfoItem videoInfoItem = new VideoInfoItem(view, this);
            videoInfoItem.setUpView(mVideoFiles.get(i), i);

            holder.mLinearLayout.addView(view);
        }
    }

    @Override
    public int getItemCount() {
        int itemsCount = mVideoFiles.size() / mItemsInRow;
        int restOfItems = mVideoFiles.size() % mItemsInRow;
        if (restOfItems > 0.0005) {
            itemsCount++;
        }
        return itemsCount;
    }

    @Override
    public void onVideoSelected(int videoPosition) {
        mEventsListener.onVideoSelectedForPlay(mVideoFiles.get(videoPosition));
    }

    @Override
    public void addAllItems(List<VideoFileInfo> videoFiles) {
        mVideoFiles.clear();
        mVideoFiles.addAll(videoFiles);
        notifyDataSetChanged();
    }
}
