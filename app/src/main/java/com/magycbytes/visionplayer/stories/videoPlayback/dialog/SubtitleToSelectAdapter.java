package com.magycbytes.visionplayer.stories.videoPlayback.dialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.magycbytes.visionplayer.DrawableIdFinder;
import com.magycbytes.visionplayer.Models.DbSubtitle;
import com.magycbytes.visionplayer.R;

import java.util.List;

/**
 * Created by magyc Bytes on 22.02.2016.
 */
@SuppressWarnings("deprecation")
class SubtitleToSelectAdapter extends BaseAdapter {
    private final List<DbSubtitle> currentMovieSubtitles;
    private final Context mContext;
    private int currentSelectedSubtitle;

    public SubtitleToSelectAdapter(List<DbSubtitle> currentMovieSubtitles, Context context) {
        this.currentMovieSubtitles = currentMovieSubtitles;
        mContext = context;
        currentSelectedSubtitle = -1;
    }

    @Override
    public int getCount() {
        return currentMovieSubtitles.size();
    }

    @Override
    public DbSubtitle getItem(int position) {
        return currentMovieSubtitles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DbSubtitle dbSubtitle = currentMovieSubtitles.get(position);

        int idResource = DrawableIdFinder.extract(dbSubtitle.getSubtitleLanguageIso(), mContext);

        View resultView = convertView;
        if (idResource < 1) {

            if (resultView == null) {
                resultView = LayoutInflater.from(mContext).inflate(R.layout.subtitle_info_at_selecting, parent, false);
            }

            markIfTheSubtitleIsDownloaded(position, resultView);

            showISOLanguage(dbSubtitle.getSubtitleLanguageIso(), resultView);
        } else {
            if (resultView == null) {
                resultView = LayoutInflater.from(mContext).inflate(R.layout.subtitle_to_select_info, parent, false);
            }

            markIfTheSubtitleIsDownloaded(position, resultView);
            showLanguageFlag(idResource, resultView);
        }
        TextView subtitleName = (TextView) resultView.findViewById(R.id.subtitleNameTextView);
        subtitleName.setText(dbSubtitle.getFileName());

        return resultView;
    }

    private void showISOLanguage(String isoLanguage, View resultView) {
        TextView subtitleLanguage = (TextView) resultView.findViewById(R.id.languageISO639TextView);
        subtitleLanguage.setText(isoLanguage.toLowerCase());
    }

    private void showLanguageFlag(int idResource, View resultView) {
        ImageView imageView = (ImageView) resultView.findViewById(R.id.imageFlagLanguage);
        Drawable flagImage = mContext.getResources().getDrawable(idResource);
        imageView.setImageDrawable(flagImage);
    }

    private void markIfTheSubtitleIsDownloaded(int position, View resultView) {
        View linearLayout = resultView.findViewById(R.id.layoutSubtitleInfoToSelect);
        if (currentSelectedSubtitle == position) {
            linearLayout.setBackgroundColor(mContext.getResources().getColor(R.color.colorDownloadedSubtitle));
        } else {
            linearLayout.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
        }
    }

    public DbSubtitle getSubtitle(int position) {
        return currentMovieSubtitles.get(position);
    }

    public void setCurrentSelectedSubtitle(int currentSelectedSubtitle) {
        this.currentSelectedSubtitle = currentSelectedSubtitle;
    }

    public void setCurrentSelectedSubtitle(DbSubtitle subtitle) {
        if (subtitle == null) return;

        for (int i = 0; i < currentMovieSubtitles.size(); ++i) {
            if (currentMovieSubtitles.get(i).getSubtitleServerId().equals(subtitle.getSubtitleServerId())) {
                currentSelectedSubtitle = i;break;
            }
        }
    }
}
