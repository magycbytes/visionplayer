package com.magycbytes.visionplayer.stories.videoPlayback.customViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.VideoView;

import com.magycbytes.visionplayer.stories.videoPlayback.models.AudioChannel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tyranul on 8/7/16.
 */

public class VideoViewEx extends VideoView implements MediaPlayer.OnPreparedListener {

    private static final String LOG_TAG = VideoViewEx.class.getSimpleName();

    private final List<AudioChannel> mMovieAudioChannels = new ArrayList<>();
    private VideoViewEvents mEventsListener;
    private MediaPlayer mMediaPlayer;

    // backup fields
    private int mCurrentAudioTrack = -1;


    public VideoViewEx(Context context) {
        super(context);
        init();
    }

    public VideoViewEx(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VideoViewEx(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private VideoViewEx(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    public void pause() {
        super.pause();
        mEventsListener.onVideoIsPaused();
    }

    @Override
    public void start() {
        super.start();
        mEventsListener.onVideoIsStarted();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mMediaPlayer = mediaPlayer;

        int duration = mediaPlayer.getDuration();
        mEventsListener.onVideoDurationAvailable(duration);

        try {
            if (mCurrentAudioTrack != -1) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    setAudioTrack(mCurrentAudioTrack);
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                extractAudioChannels(mediaPlayer);
            }
        } catch (RuntimeException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }
    }

    public void setVolumeLevel(float volume) {
        if (mMediaPlayer != null) {
            mMediaPlayer.setVolume(volume, volume);
        } else {
            Log.w(this.toString(), "Attempt to set volume on null media player");
        }
    }

    public void setEventsListener(VideoViewEvents eventsListener) {
        mEventsListener = eventsListener;
    }

    public ArrayList<AudioChannel> getMovieAudioChannels() {
        return (ArrayList<AudioChannel>) mMovieAudioChannels;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    public int getCurrentAudioTrack() {
        if (mMediaPlayer == null) return -1;

        int selectedTrack = mCurrentAudioTrack;
        try {
            selectedTrack = mMediaPlayer.getSelectedTrack(MediaPlayer.TrackInfo.MEDIA_TRACK_TYPE_AUDIO);
        } catch (IllegalStateException e) {
            Log.w(this.toString(), "On get current audio track: " + e.getMessage());
        } catch (RuntimeException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }
        return selectedTrack;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setAudioTrack(int audioTrackIndex) {
        mCurrentAudioTrack = audioTrackIndex;
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            try {
                mMediaPlayer.selectTrack(audioTrackIndex);
            } catch (RuntimeException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }

    private void init() {
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mEventsListener.onTogglePanelVisibility();
                return false;
            }
        });
        setOnPreparedListener(this);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void extractAudioChannels(MediaPlayer mediaPlayer) {
        if (mediaPlayer == null) {
            return;
        }
        mMovieAudioChannels.clear();
        MediaPlayer.TrackInfo[] currentMovieTracks = mediaPlayer.getTrackInfo();

        for (int i = 0; i < currentMovieTracks.length; ++i) {
            if (currentMovieTracks[i].getTrackType() != MediaPlayer.TrackInfo.MEDIA_TRACK_TYPE_AUDIO) {
                continue;
            }
            AudioChannel audioCanal = new AudioChannel(currentMovieTracks[i].getLanguage(), i);
            mMovieAudioChannels.add(audioCanal);
        }
    }

    public interface VideoViewEvents {
        void onTogglePanelVisibility();

        void onVideoDurationAvailable(int durationMilliseconds);

        void onVideoIsStarted();

        void onVideoIsPaused();
    }
}
