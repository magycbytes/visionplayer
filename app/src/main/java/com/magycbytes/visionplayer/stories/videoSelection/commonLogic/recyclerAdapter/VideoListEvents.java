package com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter;

import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;

/**
 * Created by Alex on 15/09/2016.
 */

public interface VideoListEvents {

    void onVideoSelectedForPlay(VideoFileInfo videoInfo);
}
