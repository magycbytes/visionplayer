package com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter;

import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;

import java.util.List;

/**
 * Created by Alex on 16/09/2016.
 */

public interface IBaseRecyclerAdapter {
    void addAllItems(List<VideoFileInfo> videoFiles);
}
