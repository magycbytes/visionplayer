package com.magycbytes.visionplayer.stories.videoPlayback.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.magycbytes.visionplayer.Database.DatabaseWriter;
import com.magycbytes.visionplayer.VisionPlayerApp;

public class SaveMoviePositionService extends IntentService {

    private static final String LOG_TAG = SaveMoviePositionService.class.getSimpleName();

    private static final String MOVIE_HASH = "movieHash";
    private static final String MOVIE_POSITION = "moviePosition";

    public SaveMoviePositionService() {
        super("SaveMoviePositionService");
    }

    public static void start(Context context, String movieHash, int moviePosition) {
        if (movieHash == null) {
            Log.d(LOG_TAG, "Trying to save video position for movie with null hash");
            return;
        }
        if (moviePosition < 0) {
            Log.d(LOG_TAG, "Trying to save video position that is smaller than 0");
            return;
        }
        Intent intent = new Intent(context, SaveMoviePositionService.class);
        intent.putExtra(MOVIE_HASH, movieHash);
        intent.putExtra(MOVIE_POSITION, moviePosition);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String movieHash = intent.getStringExtra(MOVIE_HASH);
        int moviePosition = intent.getIntExtra(MOVIE_POSITION, 0);

        DatabaseWriter databaseWriter = new DatabaseWriter(VisionPlayerApp.sMoviesSqlHelper.getWritableDatabase());
        databaseWriter.saveMoviePosition(movieHash, moviePosition);
        Log.d(this.toString(), "Finished saving movie position: " + moviePosition);
    }
}
