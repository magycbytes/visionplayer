package com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoInfoViewHolder;

import java.util.List;

/**
 * Created by Alex on 15/09/2016.
 */

public class BaseRecyclerAdapter extends RecyclerView.Adapter<VideoInfoViewHolder> implements
        VideoInfoViewHolder.VideoInfoHolderEvents, IBaseRecyclerAdapter {

    private final List<VideoFileInfo> mVideoFiles;
    private final VideoListEvents mEventsListener;

    public BaseRecyclerAdapter(List<VideoFileInfo> videoFiles, VideoListEvents eventsListener) {
        mVideoFiles = videoFiles;
        mEventsListener = eventsListener;
    }

    @Override
    public VideoInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_video_info, parent, false);
        return new VideoInfoViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(VideoInfoViewHolder holder, int position) {
        holder.show(mVideoFiles.get(position));
    }

    @Override
    public int getItemCount() {
        return mVideoFiles.size();
    }

    @Override
    public void onItemClicked(int itemPosition) {
        mEventsListener.onVideoSelectedForPlay(mVideoFiles.get(itemPosition));
    }

    @Override
    public void addAllItems(List<VideoFileInfo> videoFiles) {
        mVideoFiles.clear();
        mVideoFiles.addAll(videoFiles);
        notifyDataSetChanged();
    }
}
