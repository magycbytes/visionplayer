package com.magycbytes.visionplayer.stories.videoPlayback.customViews;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.util.Log;

/**
 * Created by Alex on 07/11/2016.
 */

public class VolumeChangeObserver extends ContentObserver {

    private static final String LOG_TAG = VolumeChangeObserver.class.getSimpleName();

    private AudioManager mAudioManager;
    private VolumeChangeObserverEvents mEventsListener;

    public VolumeChangeObserver(Context context, VolumeChangeObserverEvents eventsListener) {
        super(new Handler());

        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mEventsListener = eventsListener;
    }

    @Override
    public boolean deliverSelfNotifications() {
        return false;
    }

    @Override
    public void onChange(boolean selfChange) {
        int streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        Log.d(LOG_TAG, "Stream volume changed to : " + streamVolume);
        mEventsListener.onVolumeChangedTo(streamVolume);
    }

    public interface VolumeChangeObserverEvents {
        void onVolumeChangedTo(int volumeValue);
    }
}
