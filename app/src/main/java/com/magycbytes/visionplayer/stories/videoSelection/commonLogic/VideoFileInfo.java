package com.magycbytes.visionplayer.stories.videoSelection.commonLogic;

/**
 * Created by tyranul on 3/17/16.
 */
public class VideoFileInfo {

    private String mFilePath;
    private String mFileName;

    public VideoFileInfo(String filePath, String fileName) {
        mFilePath = filePath;
        mFileName = fileName;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        this.mFilePath = filePath;
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String fileName) {
        this.mFileName = fileName;
    }


}
