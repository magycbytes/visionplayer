package com.magycbytes.visionplayer.stories.videoSelection.commonLogic;

import android.content.Context;

import com.magycbytes.visionplayer.AppSettings.Preferences;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.BaseRecyclerAdapter;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.IBaseRecyclerAdapter;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.VideoListEvents;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.recyclerAdapter.bookView.VideoInfoBookAdapter;

import java.util.List;

/**
 * Created by Alex on 16/09/2016.
 */

class VideoAdapterFactory {

    public static IBaseRecyclerAdapter setCurrentAdapter(BaseVideosView videosView, List<VideoFileInfo> videoFiles, Context context, VideoListEvents eventsListener) {
        switch (Preferences.getLayoutVideoCollection(context)) {
            case 0:
                return setBookAdapter(videosView, videoFiles, context, eventsListener);
            case 1:
                return setGridAdapter(videosView, videoFiles, eventsListener);
            default:
                return null;
        }
    }

    private static IBaseRecyclerAdapter setBookAdapter(BaseVideosView videosView, List<VideoFileInfo> videoFiles, Context context, VideoListEvents eventsListener) {
        videosView.setLinearLayout();
        VideoInfoBookAdapter adapter = new VideoInfoBookAdapter(videoFiles, context, eventsListener);
        videosView.setAdapter(adapter);

        return adapter;
    }

    private static IBaseRecyclerAdapter setGridAdapter(BaseVideosView videosView, List<VideoFileInfo> videoFiles, VideoListEvents eventsListener) {
        videosView.setGridLayout();
        BaseRecyclerAdapter adapter = new BaseRecyclerAdapter(videoFiles, eventsListener);
        videosView.setAdapter(adapter);

        return adapter;
    }
}
