package com.magycbytes.visionplayer.stories.videoSelection.recentVideos;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.magycbytes.visionplayer.Database.movieAccess.MovieAccessCrud;
import com.magycbytes.visionplayer.VisionPlayerApp;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 15/09/2016.
 */

class RecentVideosLoader extends AsyncTaskLoader<List<VideoFileInfo>> {

    private List<VideoFileInfo> mResult;

    RecentVideosLoader(Context context) {
        super(context);
    }

    @Override
    public List<VideoFileInfo> loadInBackground() {
        List<VideoFileInfo> databaseVideos = MovieAccessCrud.getAllRecentVideos(VisionPlayerApp.sMoviesSqlHelper.getReadableDatabase());
        List<VideoFileInfo> resultList = new ArrayList<>();

        for (VideoFileInfo info : databaseVideos) {
            if (new File(info.getFilePath()).exists()) resultList.add(info);
        }

        return resultList;
    }

    @Override
    public void deliverResult(List<VideoFileInfo> data) {
        mResult = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mResult != null) deliverResult(mResult);

        if (mResult == null) forceLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        mResult = null;
    }
}
