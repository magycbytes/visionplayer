package com.magycbytes.visionplayer.stories.videoSelection;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.screens.videoSelectingPreference.VideoSelectingPreferenceActivity;
import com.magycbytes.visionplayer.stories.commonLogic.BaseActivity;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideoCollectionFragment;

import java.util.List;

public class VideoSelectionActivity extends BaseActivity {

    private static final int VIDEO_COLLECTION_READ_PERMISSION = 1;
    private static final int REQUEST_CODE_PREFERENCES = 12;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_selection);

        sendOpenActivityEvent(VideoSelectionActivity.class.getSimpleName());

        new VideoSelectionView(this);

        requestReadPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != VIDEO_COLLECTION_READ_PERMISSION) return;

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            refreshDataInAllFragments();
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.video_selecting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_rescan:
                refreshDataInAllFragments();
                return true;
            case R.id.menu_settings:
                VideoSelectingPreferenceActivity.startForResult(this, REQUEST_CODE_PREFERENCES);
                return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != REQUEST_CODE_PREFERENCES || resultCode != RESULT_OK) {
            return;
        }
        resetLayoutInAllFragments();
    }

    private void requestReadPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, VIDEO_COLLECTION_READ_PERMISSION);
        } else {
            refreshDataInAllFragments();
        }
    }

    private void refreshDataInAllFragments() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments == null) {
            return;
        }
        for (int i = 0; i < fragments.size(); ++i) {
            if (fragments.get(i) instanceof BaseVideoCollectionFragment) {
                BaseVideoCollectionFragment collectionFragment = (BaseVideoCollectionFragment) fragments.get(i);
                collectionFragment.loadFromDatabase();
            }
        }
    }

    private void resetLayoutInAllFragments() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (int i = 0; i < fragments.size(); ++i) {
            if (fragments.get(i) instanceof BaseVideoCollectionFragment) {
                BaseVideoCollectionFragment collectionFragment = (BaseVideoCollectionFragment) fragments.get(i);
                collectionFragment.resetLayout();
            }
        }
    }
}
