package com.magycbytes.visionplayer.stories.videoSelection.commonLogic;


import android.support.v4.app.Fragment;

public abstract class BaseVideoCollectionFragment extends Fragment {

    public abstract void loadFromDatabase();

    public abstract void resetLayout();

}
