package com.magycbytes.visionplayer.stories.videoPlayback.subtitles;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.magycbytes.visionplayer.Models.DbSubtitle;
import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.subtitles.SrtParser;
import com.magycbytes.visionplayer.subtitles.SubtitleText;
import com.magycbytes.visionplayer.subtitles.SubtitleTimeChecker;
import com.magycbytes.visionplayer.subtitles.SubtitleToShow;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by tyranul on 8/14/16.
 */

public class SubtitlePresenter implements SubtitleView.SubtitleViewEvents {
    private final Context mContext;
    private List<SubtitleText> mExtractedSubtitles;
    private final SubtitleView mSubtitleView;
    private DbSubtitle mCurrentSubtitle;

    public SubtitlePresenter(Context context, SubtitleView subtitleView) {
        mContext = context;
        mSubtitleView = subtitleView;

        mSubtitleView.setEventsListener(this);
    }

    public void showSubtitleFile(DbSubtitle dbSubtitle) {
        mCurrentSubtitle = dbSubtitle;
        File file = new File(mContext.getFilesDir(), dbSubtitle.getFileName());

        try {
            mExtractedSubtitles = SrtParser.parse(file.getAbsolutePath(), dbSubtitle.getEncoding());
        } catch (IOException e) {
            Toast.makeText(mContext, R.string.error_extracting_info_subtitle_file, Toast.LENGTH_LONG).show();
            Log.e(this.toString(), e.getMessage(), e);
        }
        onShowNextSubtitle();
    }

    public void reloadSubtitlesPosition() {
        if (mExtractedSubtitles == null) {
            return;
        }
        mSubtitleView.stopChangers();
        onShowNextSubtitle();
    }

    public void pauseChangers() {
        mSubtitleView.stopChangers();
    }

    private SubtitleToShow getSubtitleToShow() {
        if (videoIsNotStarted(mSubtitleView.getCurrentVideoPosition())) {
            SubtitleToShow subtitleToShow = new SubtitleToShow();
            subtitleToShow.setNextTimeToCheck(100);
            return subtitleToShow;
        }

        int nextSubtitleIndex = 0;

        SubtitleTimeChecker subtitleTimeChecker = new SubtitleTimeChecker(mSubtitleView.getCurrentVideoPosition());
        for (SubtitleText currentSubtitle : mExtractedSubtitles) {
            nextSubtitleIndex++;
            subtitleTimeChecker.setCurrentSubtitle(currentSubtitle);

            if (nextSubtitleIndex < mExtractedSubtitles.size()) {
                subtitleTimeChecker.setNextSubtitle(mExtractedSubtitles.get(nextSubtitleIndex));
            }

            if (subtitleTimeChecker.isCurrentSubtitleOld()) {
                continue;
            }

            SubtitleToShow subtitleToShow = new SubtitleToShow();

            if (subtitleTimeChecker.isNoSubtitleForThisVideoTime()) {
                subtitleToShow.setNextTimeToCheck(subtitleTimeChecker.getNextTimeToCheckForNoSubtitle());
            } else {
                subtitleToShow.setNextTimeToCheck(subtitleTimeChecker.getNextTimeToCheck());
                subtitleToShow.setIntervalToShow(subtitleTimeChecker.getIntervalToShowSubtitle());

                subtitleToShow.setPhraseText(currentSubtitle.getPhraseText());
            }

            return subtitleToShow;
        }
        return null;
    }

    @Override
    public void onShowNextSubtitle() {
        mSubtitleView.showSubtitle(getSubtitleToShow());
    }

    private boolean videoIsNotStarted(long videoCurrentPosition) {
        return videoCurrentPosition < 1;
    }

    public DbSubtitle getCurrentSubtitle() {
        return mCurrentSubtitle;
    }
}
