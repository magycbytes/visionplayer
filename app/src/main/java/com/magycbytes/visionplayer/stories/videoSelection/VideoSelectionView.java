package com.magycbytes.visionplayer.stories.videoSelection;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.stories.videoSelection.adapter.VideoSelectingAdapter;

/**
 * Created by Alex on 14/09/2016.
 */

class VideoSelectionView {

    VideoSelectionView(AppCompatActivity activity) {
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        ViewPager viewPager = (ViewPager) activity.findViewById(R.id.container);
        viewPager.setAdapter(new VideoSelectingAdapter(activity.getSupportFragmentManager(), activity));

        TabLayout tabLayout = (TabLayout) activity.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }
}
