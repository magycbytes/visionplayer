package com.magycbytes.visionplayer.stories.videoPlayback;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.magycbytes.visionplayer.Models.CurrentMovie;
import com.magycbytes.visionplayer.Models.DbSubtitle;
import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.screens.downloadSubtitles.DownloadSubtitlesActivity;
import com.magycbytes.visionplayer.stories.videoPlayback.customViews.PlaybackControlPanelView;
import com.magycbytes.visionplayer.stories.videoPlayback.dialog.AudioChannelsDialog;
import com.magycbytes.visionplayer.stories.videoPlayback.dialog.SubtitlesDialog;
import com.magycbytes.visionplayer.stories.videoPlayback.models.AudioChannel;
import com.magycbytes.visionplayer.stories.videoPlayback.subtitles.SubtitlePresenter;
import com.magycbytes.visionplayer.stories.videoPlayback.subtitles.SubtitleView;
import com.magycbytes.visionplayer.tools.FileUtilities;

import java.util.ArrayList;

public class VideoPlaybackActivity extends AppCompatActivity implements AudioChannelsDialog.AudioChannelsDialogEvents, SubtitlesDialog.SubtitleDialogEvents {

    private static final String CURRENT_AUDIO_CHANNEL_INDEX = "currentAudioChannelIndex";
    private static final String CURRENT_SUBTITLE_SERVER_ID = "currentSubtitleServerId";
    private VideoPlaybackPresenter mPresenter;
    private VideoPlaybackView mVideoPlaybackView;
    private SubtitlePresenter mSubtitlePresenter;

    public static void start(Context context, String moviePath) {
        Intent intent = new Intent(context, VideoPlaybackActivity.class);
        intent.setData(Uri.parse(moviePath));
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isLandscapeOrientation()) {
            setFullScreen();
            setContentView(R.layout.activity_video_playback);
            hideActionbar();
        } else {
            setContentView(R.layout.activity_video_playback);
        }

        mVideoPlaybackView = new VideoPlaybackView(findViewById(R.id.activity_video_playback));

        PlaybackControlPanelView panelView = (PlaybackControlPanelView) findViewById(R.id.control_panel);
        applyVolumeConfiguration(panelView);

        SubtitleView subtitleView = new SubtitleView(findViewById(R.id.activity_video_playback));
        mSubtitlePresenter = new SubtitlePresenter(this, subtitleView);
        mPresenter = new VideoPlaybackPresenter(panelView, mVideoPlaybackView, this, getSupportLoaderManager(), mSubtitlePresenter);
    }

    private boolean isLandscapeOrientation() {
        boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        Log.d(this.toString(), "Is landscape orientation: " + isLandscape);
        return isLandscape;
    }

    @Override
    protected void onPause() {
        mPresenter.onPause();
        mPresenter.saveMoviePositionInDatabase();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.startPlaying(getIntent().getData());
        mPresenter.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.video_player_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.select_audio_channel:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    showSelectAudioChannelDialog();
                }
                return true;
            case R.id.select_subtitle_menu:
                showSelectSubtitleDialog();
                return true;
            case R.id.download_subtitles:
                String filePath = getIntent().getData().getPath();
                CurrentMovie currentMovie = new CurrentMovie(mPresenter.getMovieHash(), filePath, FileUtilities.getFileSize(filePath));
                DownloadSubtitlesActivity.show(this, currentMovie);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            outState.putInt(CURRENT_AUDIO_CHANNEL_INDEX, mVideoPlaybackView.getCurrentAudioTrack());
        }
        DbSubtitle currentSubtitle = mSubtitlePresenter.getCurrentSubtitle();
        if (currentSubtitle != null) {
            outState.putString(CURRENT_SUBTITLE_SERVER_ID, currentSubtitle.getSubtitleServerId());
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mVideoPlaybackView.setAudioTrack(savedInstanceState.getInt(CURRENT_AUDIO_CHANNEL_INDEX));
        }
        mPresenter.setCurrentSubtitleServerId(savedInstanceState.getString(CURRENT_SUBTITLE_SERVER_ID));
    }

    private void setFullScreen() {
        Log.d(this.toString(), "Setting fullscreen");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void hideActionbar() {
        if (getActionBar() != null) {
            getActionBar().hide();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    private void applyVolumeConfiguration(PlaybackControlPanelView panelView) {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        panelView.configureVolume(maxVolume, currentVolume);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showSelectAudioChannelDialog() {
        int currentAudioTrackIndex = mVideoPlaybackView.getCurrentAudioTrack();
        ArrayList<AudioChannel> audioChannels = mVideoPlaybackView.getAudioChannels();
        int currentTrackArrayIndex = 0;
        for (int i = 0; i < audioChannels.size(); ++i) {
            if (audioChannels.get(i).getTrackIndex() == currentAudioTrackIndex) {
                currentTrackArrayIndex = i;
                break;
            }
        }
        AudioChannelsDialog dialog = AudioChannelsDialog.create(audioChannels, currentTrackArrayIndex, this);
        dialog.show(getSupportFragmentManager(), "");
    }

    private void showSelectSubtitleDialog() {
        ArrayList<DbSubtitle> movieSubtitles = (ArrayList<DbSubtitle>) mPresenter.getMovieSubtitles();
        if (movieSubtitles == null || movieSubtitles.isEmpty()) {
            Toast.makeText(this, "No subtitles found", Toast.LENGTH_SHORT).show();
            return;
        }
        DialogFragment dialogFragment = SubtitlesDialog.create(movieSubtitles, mSubtitlePresenter.getCurrentSubtitle(), this);
        dialogFragment.show(getSupportFragmentManager(), "");
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onAudioSelected(int index) {
        mVideoPlaybackView.setAudioTrack(index);
    }

    @Override
    public void onSubtitleSelected(DbSubtitle subtitle) {
        mSubtitlePresenter.showSubtitleFile(subtitle);
    }
}
