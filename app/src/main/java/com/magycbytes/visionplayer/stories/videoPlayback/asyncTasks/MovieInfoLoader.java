package com.magycbytes.visionplayer.stories.videoPlayback.asyncTasks;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.magycbytes.visionplayer.Database.DatabaseFacade;
import com.magycbytes.visionplayer.Database.MovieInfoDb;
import com.magycbytes.visionplayer.stories.videoPlayback.tools.HashCalculator;

import java.io.IOException;

/**
 * Created by Alex on 07/09/2016.
 */

public class MovieInfoLoader extends AsyncTaskLoader<MovieInfoDb> {

    private static final String LOG_TAG = MovieInfoLoader.class.getSimpleName();

    private final String mFilePath;
    private MovieInfoDb mExtractMovieInfo;

    public MovieInfoLoader(String filePath, Context context) {
        super(context);

        mFilePath = filePath;
    }

    @Override
    public MovieInfoDb loadInBackground() {
        try {
            String hash = HashCalculator.computeHash(mFilePath);
            DatabaseFacade databaseFacade = new DatabaseFacade();
            return databaseFacade.extractMovieInformation(hash);
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void deliverResult(MovieInfoDb data) {
        mExtractMovieInfo = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mExtractMovieInfo != null) deliverResult(mExtractMovieInfo);
        else forceLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mExtractMovieInfo != null) mExtractMovieInfo = null;
    }
}
