package com.magycbytes.visionplayer.stories.commonLogic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Alex on 20/09/2016.
 */

public class BaseActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    protected void sendOpenActivityEvent(String screenName) {
        Bundle bundle = new Bundle();
        bundle.putString("Screen_Name", screenName);
        mFirebaseAnalytics.logEvent("View_Screen", bundle);
    }
}
