package com.magycbytes.visionplayer.stories.videoSelection.recentVideos;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideoViewPresenter;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.BaseVideosView;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;

import java.util.List;

/**
 * Created by Alex on 15/09/2016.
 */

class RecentVideosPresenter extends BaseVideoViewPresenter implements LoaderManager.LoaderCallbacks<List<VideoFileInfo>> {


    RecentVideosPresenter(BaseVideosView view, LoaderManager loaderManager, Context context) {
        super(view, loaderManager, context);
    }

    @Override
    public void loadFromDatabase() {
        mLoaderManager.restartLoader(0, null, this);
    }

    @Override
    public Loader<List<VideoFileInfo>> onCreateLoader(int id, Bundle args) {
        mView.showProgress();
        return new RecentVideosLoader(mContext);
    }

    @Override
    public void onLoadFinished(Loader<List<VideoFileInfo>> loader, List<VideoFileInfo> data) {
        if (loader.getId() != 0) return;

        mView.hideProgress();

        if (data.isEmpty()) mView.showEmptyList();
        else mView.hideEmptyList();

        mAdapter.addAllItems(data);
    }

    @Override
    public void onLoaderReset(Loader<List<VideoFileInfo>> loader) {

    }
}
