package com.magycbytes.visionplayer.stories.videoSelection.commonLogic;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.screens.videoSelecting.helpers.ColumnsCalculator;

/**
 * Created by Alex on 15/09/2016.
 */

public class BaseVideosView implements View.OnClickListener {

    private BaseVideoViewEvents mEventsListener;

    private final RecyclerView mRecyclerView;
    private final View mEmptyListIndicator;
    private final ProgressBar mProgressBar;


    public BaseVideosView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.video_list);
        mEmptyListIndicator = rootView.findViewById(R.id.empty_list_indicator);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.database_progress);

        Button retryDatabase = (Button) rootView.findViewById(R.id.refresh_database);
        retryDatabase.setOnClickListener(this);
    }

    void setGridLayout() {
        Context context = mRecyclerView.getContext();
        mRecyclerView.setLayoutManager(new GridLayoutManager(context, ColumnsCalculator.getNumberOfColumns(context)));
    }

    void setLinearLayout() {
        Context context = mRecyclerView.getContext();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        mRecyclerView.setAdapter(adapter);
    }

    public void showEmptyList() {
        mEmptyListIndicator.setVisibility(View.VISIBLE);
    }

    public void hideEmptyList() {
        mEmptyListIndicator.setVisibility(View.GONE);
    }

    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (mEventsListener != null) mEventsListener.onRetryDatabase();
    }

    public void setEventsListener(BaseVideoViewEvents eventsListener) {
        mEventsListener = eventsListener;
    }

    interface BaseVideoViewEvents {
        void onRetryDatabase();
    }
}
