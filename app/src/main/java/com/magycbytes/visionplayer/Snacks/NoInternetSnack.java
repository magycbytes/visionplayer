package com.magycbytes.visionplayer.Snacks;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by alexandru on 28/06/16.
 */
public class NoInternetSnack {

    private final Snackbar mSnackBar;
    private final OnRetryEvent mOnRetryEventListener;

    public NoInternetSnack(View view, OnRetryEvent onRetryEventListener) {
        mOnRetryEventListener = onRetryEventListener;
        if (mOnRetryEventListener == null) {
            throw new IllegalArgumentException("listener can't be null");
        }

        mSnackBar = Snackbar.make(view, "No internet connection", Snackbar.LENGTH_INDEFINITE);
        mSnackBar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnRetryEventListener.retryClicked();
            }
        });
    }

    public void show() {
        mSnackBar.show();
    }

    public interface OnRetryEvent {
        void retryClicked();
    }
}
