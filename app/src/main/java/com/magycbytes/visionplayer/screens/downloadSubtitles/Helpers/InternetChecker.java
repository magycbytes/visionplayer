package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;

import com.magycbytes.visionplayer.Snacks.NoInternetSnack;

/**
 * Created by alexandru on 28/06/16.
 */
class InternetChecker implements NoInternetSnack.OnRetryEvent {

    private final Context mContext;
    private OnInternetChecked mOnInternetChecked;
    private final View mRootView;

    InternetChecker(View rootView, OnInternetChecked onInternetChecked, Context context) {
        mRootView = rootView;
        mContext = context;
        if (onInternetChecked == null) {
            throw new IllegalArgumentException("on internet checked can't be null");
        }
        mOnInternetChecked = onInternetChecked;
    }

    void checkInternet() {
        if (isInternetAvailable()) {
            mOnInternetChecked.continueTask();
        } else {
            NoInternetSnack noInternetSnack = new NoInternetSnack(mRootView, this);
            noInternetSnack.show();
        }
    }

    private boolean isInternetAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void retryClicked() {
        checkInternet();
    }

    interface OnInternetChecked {
        void continueTask();
    }
}
