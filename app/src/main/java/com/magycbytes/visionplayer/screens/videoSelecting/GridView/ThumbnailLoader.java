package com.magycbytes.visionplayer.screens.videoSelecting.GridView;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by alexandru on 6/11/16.
 */
public class ThumbnailLoader extends AsyncTask<Void, Void, Integer> {

    private final ImageView mTarget;
    private final String mFilePath;
    private Bitmap mBitmap;

    public ThumbnailLoader(ImageView target, String filePath) {
        mTarget = target;
        mFilePath = filePath;
    }

    public static void start(Context context, String filePath, ImageView into) {
        new Picasso.Builder(context)
                .addRequestHandler(new ThumbnailRequestHandler())
                .build()
                .load(filePath)
                .into(into);
    }

    @Override
    protected Integer doInBackground(Void... params) {
        mBitmap = ThumbnailUtils.createVideoThumbnail(mFilePath,
                MediaStore.Images.Thumbnails.MINI_KIND);
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        mTarget.setImageBitmap(mBitmap);
    }
}
