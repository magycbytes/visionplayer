package com.magycbytes.visionplayer.screens.videoSelecting.helpers;

import android.content.Context;

import com.magycbytes.visionplayer.R;

/**
 * Created by alexandru on 6/11/16.
 */
public class ColumnsCalculator {

    public static int getNumberOfColumns(Context context) {
        int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
        int thumbnailWidth = (int) context.getResources().getDimension(R.dimen.thumbnailWidth);
        int thumbnailsMargins = (int) context.getResources().getDimension(R.dimen.marginThumbnails);
        thumbnailsMargins *= 2;
        return (widthPixels - thumbnailsMargins) / thumbnailWidth;
    }
}
