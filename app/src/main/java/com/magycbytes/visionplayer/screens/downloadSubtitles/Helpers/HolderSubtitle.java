package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.visionplayer.DrawableIdFinder;
import com.magycbytes.visionplayer.R;
import com.squareup.picasso.Picasso;

/**
 * Created by alexandru on 30/06/16.
 */
public class HolderSubtitle extends RecyclerView.ViewHolder {

    private final Context mContext;
    private final ImageView mCountryFlag;
    private final TextView mSubtitleName;
    private final TextView mLanguageName;
    private final View mRootLayout;
    private OnItemClick mItemClickListener;

    HolderSubtitle(View itemView, OnItemClick itemClickListener) {
        super(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemClickListener.subtitleClicked(getAdapterPosition());
            }
        });

        mItemClickListener = itemClickListener;
        mContext = itemView.getContext();

        mRootLayout = itemView.findViewById(R.id.rootLayout);
        mCountryFlag = (ImageView) itemView.findViewById(R.id.countryFlag);
        mSubtitleName = (TextView) itemView.findViewById(R.id.subtitleName);
        mLanguageName = (TextView) itemView.findViewById(R.id.subtitleLanguage);
    }

    void showSubtitle(Subtitle subtitle) {
        mSubtitleName.setText(subtitle.getSubtitleFileName());
        mLanguageName.setText(subtitle.getLanguageLongName());
        applyFlagCountry(subtitle.getLanguageIso639());
        checkBackgroundColor(subtitle.isDownloaded());
    }

    private void applyFlagCountry(String isoLanguage) {
        int flagImageIdentifier = DrawableIdFinder.extract(isoLanguage, mContext);

        if (flagImageIdentifier < 1) {
            flagImageIdentifier = R.drawable.ic_place_holder_image;
        }

        Picasso.with(mContext)
                .load(flagImageIdentifier)
                .into(mCountryFlag);
    }

    private void checkBackgroundColor(boolean isSubtitleDownloaded) {
        int rootBackgroundColor;
        int textColor = Color.WHITE;

        if (isSubtitleDownloaded) {
            rootBackgroundColor = ContextCompat.getColor(mContext, R.color.colorDownloadedSubtitle);
        } else {
            rootBackgroundColor = ContextCompat.getColor(mContext, R.color.colorSubtitleNotDownloaded);
            textColor = Color.BLACK;
        }

        mSubtitleName.setTextColor(textColor);
        mLanguageName.setTextColor(textColor);

        mRootLayout.setBackgroundColor(rootBackgroundColor);
    }

    public interface OnItemClick {
        void subtitleClicked(int subtitlePosition);
    }
}
