package com.magycbytes.visionplayer.screens.videoSelectingPreference;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;

import com.magycbytes.visionplayer.R;

/**
 * Created by alexandru on 6/10/16.
 */
class LayoutPreferencePagerAdapter extends FragmentPagerAdapter {

    private static final int PAGE_NUMBER = 2;

    private final Context mContext;

    public LayoutPreferencePagerAdapter(FragmentManager fm, Context mContext) {
        super(fm);
        this.mContext = mContext;
    }

    @Override
    public Fragment getItem(int position) {
        LayoutPreferenceFragment fragment = new LayoutPreferenceFragment();
        switch (position) {
            case 0:
                fragment.setPreviewImage(ContextCompat.getDrawable(mContext, R.drawable.preview_book_layout));
                break;
            case 1:
                fragment.setPreviewImage(ContextCompat.getDrawable(mContext, R.drawable.preview_grid_video_collection));
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_NUMBER;
    }
}
