package com.magycbytes.visionplayer.screens.videoSelectingPreference;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.magycbytes.visionplayer.AppSettings.Preferences;
import com.magycbytes.visionplayer.R;

@SuppressWarnings("WeakerAccess")
public class VideoSelectingPreferenceActivity extends AppCompatActivity {

    protected ViewPager mViewPager;
    protected Button mSavePresentingLayout;
    protected Toolbar mToolbar;

    public static void startForResult(Activity activity, int code) {
        Intent intent = new Intent(activity, VideoSelectingPreferenceActivity.class);
        activity.startActivityForResult(intent, code);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_selecting_preference);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mSavePresentingLayout = (Button) findViewById(R.id.savePresentingLayout);
        mSavePresentingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePresentingLayout();
            }
        });
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setUpView();
    }

    protected void savePresentingLayout() {
        Preferences.saveVideoCollectionLayout(this, mViewPager.getCurrentItem());
        setResult(RESULT_OK);
        finish();
    }

    private void setUpView() {
        setSupportActionBar(mToolbar);

        LayoutPreferencePagerAdapter adapter = new LayoutPreferencePagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(adapter);

        mViewPager.setCurrentItem(Preferences.getLayoutVideoCollection(this));
    }
}
