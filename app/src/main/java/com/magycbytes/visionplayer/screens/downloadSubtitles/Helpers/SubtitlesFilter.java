package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers;

import android.os.AsyncTask;

import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.visionplayer.Database.DatabaseFacade;
import com.magycbytes.visionplayer.Database.MovieInfoDb;
import com.magycbytes.visionplayer.Models.DbSubtitle;
import com.magycbytes.visionplayer.screens.downloadSubtitles.DownloadSubtitlePresenter;

import java.util.List;

/**
 * Created by alexandru on 01/07/16.
 */
class SubtitlesFilter extends AsyncTask<Void, Void, Integer> {

    private final DownloadSubtitlePresenter mSubtitlePresenter;
    private final List<Subtitle> mSubtitles;
    private final String mMovieHash;

    SubtitlesFilter(DownloadSubtitlePresenter subtitlePresenter, List<Subtitle> subtitles, String movieHash) {
        mSubtitlePresenter = subtitlePresenter;
        mSubtitles = subtitles;
        mMovieHash = movieHash;
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        DatabaseFacade databaseFacade = new DatabaseFacade();

        MovieInfoDb information = databaseFacade.extractMovieInformation(mMovieHash);
        List<DbSubtitle> movieExistingSubtitles = information.getCurrentMovieSubtitle();

        for (int i = 0; i < movieExistingSubtitles.size(); ++i) {
            String idSubtitle = movieExistingSubtitles.get(i).getSubtitleServerId();
            for (int j = 0; j < mSubtitles.size(); ++j) {
                if (mSubtitles.get(j).getIdSubtitleFile().equals(idSubtitle)) {
                    mSubtitles.get(j).setDownloaded();
                    break;
                }
            }
        }
        return 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        mSubtitlePresenter.notifySubtitleListModified();
    }
}
