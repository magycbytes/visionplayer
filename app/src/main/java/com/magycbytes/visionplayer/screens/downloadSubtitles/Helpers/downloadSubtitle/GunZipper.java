package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.downloadSubtitle;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

/**
 * Created by tyranul on 2/10/16.
 */
class GunZipper {

    public static byte[] decodeData(byte[] dataToDecode) throws IOException {

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(dataToDecode);
        GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream, 32);


        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(dataToDecode.length);
        byte[] unzippedDataPart = new byte[32];
        int bytesRead;
        while ((bytesRead = gzipInputStream.read(unzippedDataPart)) != -1) {
            byteArrayOutputStream.write(unzippedDataPart, 0, bytesRead);
        }
        gzipInputStream.close();
        byteArrayInputStream.close();

        return byteArrayOutputStream.toByteArray();
    }
}