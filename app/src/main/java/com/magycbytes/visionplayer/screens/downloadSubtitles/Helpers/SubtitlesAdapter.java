package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.visionplayer.R;

import java.util.List;

/**
 * Created by alexandru on 30/06/16.
 */
public class SubtitlesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Subtitle> mSubtitles;
    private final HolderSubtitle.OnItemClick mItemClickListener;

    public SubtitlesAdapter(List<Subtitle> subtitles, HolderSubtitle.OnItemClick itemClickListener) {
        mSubtitles = subtitles;
        mItemClickListener = itemClickListener;
    }

    public Subtitle getSubtitle(int position) {
        return mSubtitles.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_subtitle_info, parent, false);
        return new HolderSubtitle(view, mItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HolderSubtitle holderSubtitle = (HolderSubtitle) holder;
        holderSubtitle.showSubtitle(mSubtitles.get(position));
    }

    @Override
    public int getItemCount() {
        return mSubtitles.size();
    }
}
