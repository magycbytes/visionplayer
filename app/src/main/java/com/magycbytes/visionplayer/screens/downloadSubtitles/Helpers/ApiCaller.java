package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.magycbytes.opensubtitlelibrary.Callbacks.DownloadSubtitleCallback;
import com.magycbytes.opensubtitlelibrary.Callbacks.LoginCallback;
import com.magycbytes.opensubtitlelibrary.Callbacks.SearchSubtitleCallback;
import com.magycbytes.opensubtitlelibrary.Callbacks.SubtitleLanguagesCallback;
import com.magycbytes.opensubtitlelibrary.Models.Network.LoginProfile;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodCall;
import com.magycbytes.opensubtitlelibrary.Models.Network.MethodResponse;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.DownloadedSubtitle;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.SubtitleLanguage;
import com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.MethodGenerator;
import com.magycbytes.opensubtitlelibrary.Network.MethodBodyGenerator.SearchSubtitleParameters;
import com.magycbytes.opensubtitlelibrary.Network.NetworkApiManager;
import com.magycbytes.visionplayer.Database.DatabaseFacade;
import com.magycbytes.visionplayer.Models.CurrentMovie;
import com.magycbytes.visionplayer.screens.downloadSubtitles.DownloadSubtitlePresenter;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.downloadSubtitle.NameGenerator;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.downloadSubtitle.SubtitleDeviceSaver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;


/**
 * Created by alexandru on 28/06/16.
 */
public class ApiCaller
        implements
        SearchSubtitleCallback.OnSearchSubtitle,
        LoginCallback.OnLogin, DownloadSubtitleCallback.OnDownload,
        InternetChecker.OnInternetChecked,
        SubtitleLanguagesCallback.OnLanguagesExtract {

    public static final int TASK_GET_LANGUAGES = 0;
    public static final int TASK_LOGIN = 1;
    public static final int TASK_SEARCH_SUBTITLES = 2;
    public static final int TASK_DOWNLOAD_SUBTITLE = 3;

    private final InternetChecker mInternetChecker;
    private final DownloadSubtitlePresenter mSubtitlePresenter;
    private final Context mContext;
    private final CurrentMovie mCurrentMovie;
    private int mTaskToExecute = 0;
    private String mAccessToken;
    private Subtitle mSubtitleIdToDownload;
    private Call<MethodResponse> mDownloadSubtitleCall;
    private boolean mNewSubtitleDownloaded = false;

    public ApiCaller(View rootView, DownloadSubtitlePresenter subtitlePresenter, CurrentMovie currentMovie) {
        mSubtitlePresenter = subtitlePresenter;
        mCurrentMovie = currentMovie;
        mContext = rootView.getContext();

        mInternetChecker = new InternetChecker(rootView, this, rootView.getContext());
    }

    public void executeTask(int taskNumber) {
        Log.i(this.toString(), "Execute task number: " + taskNumber);
        mTaskToExecute = taskNumber;
        mInternetChecker.checkInternet();
    }

    public void stopDownloading() {
        Log.i(this.toString(), "Check if can stop downloading subtitle");
        if (mDownloadSubtitleCall != null) {
            Log.i(this.toString(), "Send signal for stopping subtitle download process");
            mDownloadSubtitleCall.cancel();
        }
    }

    private void getLanguages() {
        Log.i(this.toString(), "Send request for extracting languages");
        mTaskToExecute = TASK_GET_LANGUAGES;

        SubtitleLanguagesCallback callback = new SubtitleLanguagesCallback(this);
        MethodCall methodCall = MethodGenerator.getLanguages(Locale.getDefault().getLanguage());
        Call<MethodResponse> call = NetworkApiManager.getApi().sendRequest(methodCall);
        call.enqueue(callback);
    }

    private void login() {
        Log.i(this.toString(), "Send request for login");
        mTaskToExecute = TASK_LOGIN;

        LoginProfile loginProfile = new LoginProfile(Locale.getDefault().getLanguage());
        LoginCallback loginCallback = new LoginCallback(this);
        NetworkApiManager.getApi().sendRequest(MethodGenerator.logIn(loginProfile)).enqueue(loginCallback);
    }

    private void searchSubtitles() {
        Log.i(this.toString(), "Try to send request for searching subtitles");
        if (mAccessToken == null || mAccessToken.isEmpty()) {
            Log.e(this.toString(), "No access token");
            mTaskToExecute = TASK_LOGIN;
            error();
            return;
        }

        Log.i(this.toString(), "Start searching subtitles");
        mTaskToExecute = TASK_SEARCH_SUBTITLES;

        SearchSubtitleParameters parameters = new SearchSubtitleParameters(
                mAccessToken,
                mSubtitlePresenter.getSubtitleSelectedLanguage(),
                mCurrentMovie.getMovieHash(),
                mCurrentMovie.getMovieSize()
        );
        SearchSubtitleCallback callback = new SearchSubtitleCallback(this);
        Call<MethodResponse> call = NetworkApiManager.getApi().sendRequest(MethodGenerator.searchSubtitle(parameters));
        call.enqueue(callback);
    }

    private void downloadSubtitle() {
        mSubtitlePresenter.showDownloadSubtitleDialog();
        Log.i(this.toString(), "Start downloading subtitle");
        mTaskToExecute = TASK_DOWNLOAD_SUBTITLE;
        DownloadSubtitleCallback downloadSubtitleCallback = new DownloadSubtitleCallback(this);
        List<Integer> subtitleIds = new ArrayList<>();
        subtitleIds.add(Integer.valueOf(mSubtitleIdToDownload.getIdSubtitleFile()));
        MethodCall call = MethodGenerator.downloadSubtitle(mAccessToken, subtitleIds);
        mDownloadSubtitleCall = NetworkApiManager.getApi().sendRequest(call);
        mDownloadSubtitleCall.enqueue(downloadSubtitleCallback);
    }

    @Override
    public void subtitlesFound(List<Subtitle> subtitles) {
        Log.i(this.toString(), "Show found subtitles");
        Collections.sort(subtitles);

        SubtitlesFilter subtitlesFilter = new SubtitlesFilter(mSubtitlePresenter, subtitles, mCurrentMovie.getMovieHash());
        subtitlesFilter.execute();

        mSubtitlePresenter.showFoundSubtitles(subtitles);
        mSubtitlePresenter.setSubtitles(subtitles);
        mSubtitlePresenter.resetLanguageSelector();
    }

    @Override
    public void loginWithSuccess(String accessToken) {
        Log.i(this.toString(), "Log in with success");
        mAccessToken = accessToken;

        searchSubtitles();
    }

    @Override
    public void subtitleDownloaded(DownloadedSubtitle downloadedSubtitle) {
        String subtitleFileName = mSubtitleIdToDownload.getSubtitleFileName();
        if (!mSubtitleIdToDownload.isDownloaded()) {
            subtitleFileName = NameGenerator.generate(mContext, mSubtitleIdToDownload.getSubtitleFileName());
        }

        try {
            SubtitleDeviceSaver subtitleDeviceSaver = new SubtitleDeviceSaver(mContext);
            subtitleDeviceSaver.saveEncryptedData(downloadedSubtitle.getSubtitleContent(), subtitleFileName);

            if (!mSubtitleIdToDownload.isDownloaded()) {
                mSubtitleIdToDownload.setDownloaded();
                mSubtitleIdToDownload.setSubtitleFileName(subtitleFileName);
                mSubtitlePresenter.notifySubtitleListModified();

                DatabaseFacade databaseFacade = new DatabaseFacade();
                databaseFacade.saveDownloadedSubtitle(mSubtitleIdToDownload, mCurrentMovie.getMovieHash());

            }
            mNewSubtitleDownloaded = true;
            Toast.makeText(mContext, com.magycbytes.opensubtitlelibrary.R.string.text_subtitle_downloaded_success, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(mContext, com.magycbytes.opensubtitlelibrary.R.string.text_error_saving_subtitle, Toast.LENGTH_SHORT).show();
            if (e.getMessage() == null) {
                Log.e(this.toString(), "Exception at saving subtitle, and null error");
            } else {
                Log.e(this.toString(), e.getMessage());
            }
        } finally {
            mSubtitlePresenter.hideProgressDownloading();
        }
    }

    @Override
    public void continueTask() {
        switch (mTaskToExecute) {
            case TASK_GET_LANGUAGES:
                mSubtitlePresenter.showExecutingProgress();
                getLanguages();
                break;
            case TASK_LOGIN:
                mSubtitlePresenter.showExecutingProgress();
                login();
                break;
            case TASK_SEARCH_SUBTITLES:
                mSubtitlePresenter.showExecutingProgress();
                searchSubtitles();
                break;
            case TASK_DOWNLOAD_SUBTITLE:
                downloadSubtitle();
                break;
        }
    }

    @Override
    public void showSubtitlesLanguages(List<SubtitleLanguage> subtitleLanguages) {
        mSubtitlePresenter.showLanguages(subtitleLanguages);
    }

    @Override
    public void error() {
        Log.e(this.toString(), "Error in api call");
        mSubtitlePresenter.hideProgressDownloading();
        mSubtitlePresenter.hideExecutingProgress();
        // TODO impl event snack error
    }

    public void setSubtitleToDownload(Subtitle subtitleIdToDownload) {
        mSubtitleIdToDownload = subtitleIdToDownload;
    }

    public boolean isNewSubtitleDownloaded() {
        return mNewSubtitleDownloaded;
    }

    public void setNewSubtitleDownloaded(boolean newSubtitleDownloaded) {
        mNewSubtitleDownloaded = newSubtitleDownloaded;
    }
}
