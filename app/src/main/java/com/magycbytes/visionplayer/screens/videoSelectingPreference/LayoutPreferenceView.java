package com.magycbytes.visionplayer.screens.videoSelectingPreference;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.magycbytes.visionplayer.R;

/**
 * Created by alexandru on 6/10/16.
 */
class LayoutPreferenceView {

    private final ImageView mPreviewLayout;


    public LayoutPreferenceView(View rootView) {
        mPreviewLayout = (ImageView) rootView.findViewById(R.id.previewLayout);
    }

    public void setImagePreview(Drawable drawable) {
        mPreviewLayout.setImageDrawable(drawable);
    }
}
