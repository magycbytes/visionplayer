package com.magycbytes.visionplayer.screens.downloadSubtitles;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.SubtitleLanguage;
import com.magycbytes.visionplayer.dialogs.ExistingSubtitleDialog;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.ApiCaller;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.HolderSubtitle;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.SubtitlesAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by alexandru on 28/06/16.
 */
public class DownloadSubtitlePresenter implements AdapterView.OnItemSelectedListener, HolderSubtitle.OnItemClick {

    private final Context mContext;
    private final DownloadSubtitleView mDownloadSubtitleView;
    private List<Subtitle> mSubtitles;
    private ApiCaller mApiCaller;
    private ProgressDialog mProgressDialog;

    public DownloadSubtitlePresenter(Context context, DownloadSubtitleView downloadSubtitleView) {
        mContext = context;
        mDownloadSubtitleView = downloadSubtitleView;

        mDownloadSubtitleView.setLanguageChangeListener(this);
    }

    public void showLanguages(List<SubtitleLanguage> subtitleLanguages) {
        Collections.sort(subtitleLanguages);

        attachAllLanguagesItem(subtitleLanguages);
        setNewAdapter(subtitleLanguages);
    }

    public void showFoundSubtitles(List<Subtitle> subtitles) {
        // TODO check with subtitles from database to see and set if some subtitles are downloaded
        SubtitlesAdapter subtitlesAdapter = new SubtitlesAdapter(subtitles, this);
        mDownloadSubtitleView.setSubtitleAdapter(subtitlesAdapter);
    }

    public void resetLanguageSelector() {
        mDownloadSubtitleView.moveToAllLanguagesItem();
    }

    public String getSubtitleSelectedLanguage() {
        return mDownloadSubtitleView.getCurrentLanguageCode();
    }

    public void setSubtitles(List<Subtitle> subtitles) {
        mSubtitles = subtitles;
    }

    public void notifySubtitleListModified() {
        mDownloadSubtitleView.refreshSubtitleAdapter();
    }

    public void hideProgressDownloading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void showExecutingProgress() {
        mDownloadSubtitleView.showProgressBar();
    }

    public void hideExecutingProgress() {
        mDownloadSubtitleView.hideProgressBar();
    }

    public void setApiCaller(ApiCaller apiCaller) {
        mApiCaller = apiCaller;
    }

    private void attachAllLanguagesItem(List<SubtitleLanguage> subtitleLanguages) {
        SubtitleLanguage allSubtitles = new SubtitleLanguage(getAllLanguagesCodes(subtitleLanguages), mContext);
        subtitleLanguages.add(0, allSubtitles);
    }

    private String getAllLanguagesCodes(List<SubtitleLanguage> subtitleLanguages) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < subtitleLanguages.size(); ++i) {
            if (i != 0) {
                stringBuilder.append(",");
            }
            String iso639 = subtitleLanguages.get(i).getIso639();
            stringBuilder.append(iso639);
        }
        return stringBuilder.toString();
    }

    private void setNewAdapter(List<SubtitleLanguage> subtitleLanguages) {
        ArrayAdapter<SubtitleLanguage> anotherAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, subtitleLanguages);
        anotherAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDownloadSubtitleView.setLanguagesAdapter(anotherAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (mSubtitles == null) {
            return;
        }
        // if selected item - all languages
        if (i == 0) {
            showFoundSubtitles(mSubtitles);
            return;
        }

        List<Subtitle> subtitlesToShow = new ArrayList<>();
        String isoLanguage = mDownloadSubtitleView.getCurrentLanguageCode();
        for (Subtitle subtitle : mSubtitles) {
            if (subtitle.getLanguageIso639().equals(isoLanguage)) {
                subtitlesToShow.add(subtitle);
            }
        }
        showFoundSubtitles(subtitlesToShow);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void subtitleClicked(int subtitlePosition) {
        // TODO show progress downloading
        // TODO check if is not already downloading this item
        // TODO decide if allow to download multiple files at the same time
        // TODO MAYBE TO PUT DOWNLOAD TASK INTO INTENT SERVICE
        Subtitle subtitle = mDownloadSubtitleView.getSubtitle(subtitlePosition);
        mApiCaller.setSubtitleToDownload(subtitle);

        if (subtitle.isDownloaded()) {
            ExistingSubtitleDialog existingSubtitleDialog = new ExistingSubtitleDialog(mContext, mApiCaller);
            existingSubtitleDialog.showMessageAlreadyExistSubtitle();
            return;
        }

        mApiCaller.executeTask(ApiCaller.TASK_DOWNLOAD_SUBTITLE);
    }

    public void showDownloadSubtitleDialog() {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setTitle("Download subtitle");
        mProgressDialog.setMessage("Downloading...");
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mApiCaller.stopDownloading();
                mProgressDialog.dismiss();
            }
        });
        mProgressDialog.show();
    }
}
