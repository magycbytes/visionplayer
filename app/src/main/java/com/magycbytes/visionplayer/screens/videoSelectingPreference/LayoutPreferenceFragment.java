package com.magycbytes.visionplayer.screens.videoSelectingPreference;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.visionplayer.R;

/**
 * Created by alexandru on 6/10/16.
 */
public class LayoutPreferenceFragment extends Fragment {

    private static final String PREVIEW_IMAGE = "PreviewImage";
    private Drawable mPreviewImage;
    private LayoutPreferenceView mLayoutPreferenceView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_selecting_preference, container, false);

        mLayoutPreferenceView = new LayoutPreferenceView(view);
        mLayoutPreferenceView.setImagePreview(mPreviewImage);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Bitmap bitmap = ((BitmapDrawable) mPreviewImage).getBitmap();
        outState.putParcelable(PREVIEW_IMAGE, bitmap);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState == null) {
            return;
        }

        Bitmap bitmap = savedInstanceState.getParcelable(PREVIEW_IMAGE);
        mPreviewImage = new BitmapDrawable(getResources(), bitmap);
        mLayoutPreferenceView.setImagePreview(mPreviewImage);
    }

    public void setPreviewImage(Drawable mPreviewImage) {
        this.mPreviewImage = mPreviewImage;
    }
}
