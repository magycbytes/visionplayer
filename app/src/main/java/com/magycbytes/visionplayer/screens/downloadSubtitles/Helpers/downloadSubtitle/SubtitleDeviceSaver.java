package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.downloadSubtitle;

import android.content.Context;
import android.util.Base64;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by tyranul on 3/15/16.
 */
public class SubtitleDeviceSaver {
    private final Context mContext;
    private byte[] mDecodedData;
    private byte[] mUnzippedData;

    public SubtitleDeviceSaver(Context context) {
        mContext = context;
    }

    public void saveEncryptedData(String encryptedData, String fileName) throws IOException {
        decodeBase64(encryptedData);
        unzipData();
        writeIntoFile(NameGenerator.generate(mContext, fileName));
    }

    private void decodeBase64(String encodedBase64Data) {
        mDecodedData = Base64.decode(encodedBase64Data, Base64.DEFAULT);
    }

    private void unzipData() throws IOException {
        mUnzippedData = GunZipper.decodeData(mDecodedData);
    }

    private void writeIntoFile(String fileName) throws IOException {
        FileOutputStream outputStream;
        outputStream = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);
        outputStream.write(mUnzippedData);
        outputStream.close();
    }
}
