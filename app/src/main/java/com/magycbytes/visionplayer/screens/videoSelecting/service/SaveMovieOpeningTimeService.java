package com.magycbytes.visionplayer.screens.videoSelecting.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.magycbytes.visionplayer.Database.movieAccess.MovieAccessCrud;
import com.magycbytes.visionplayer.VisionPlayerApp;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.RecentVideo;
import com.magycbytes.visionplayer.stories.videoSelection.commonLogic.VideoFileInfo;


public class SaveMovieOpeningTimeService extends IntentService {

    private static final String LOG_TAG = SaveMovieOpeningTimeService.class.getSimpleName();


    private static final String MOVIE_FILE_PATH = "movieFilePath";
    private static final String MOVIE_FILE_NAME = "movieFileName";

    public static void save(Context context, VideoFileInfo videoFileInfo) {
        if (videoFileInfo == null) {
            Log.e(LOG_TAG, "Trying to save time for a null object");
            return;
        }

        Intent intent = new Intent(context, SaveMovieOpeningTimeService.class);
        intent.putExtra(MOVIE_FILE_PATH, videoFileInfo.getFilePath());
        intent.putExtra(MOVIE_FILE_NAME, videoFileInfo.getFileName());

        context.startService(intent);
    }

    public SaveMovieOpeningTimeService() {
        super(SaveMovieOpeningTimeService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String movieFilePath = intent.getStringExtra(MOVIE_FILE_PATH);
        String movieName = intent.getStringExtra(MOVIE_FILE_NAME);
        long openingTime = System.currentTimeMillis();

        RecentVideo recentVideo = new RecentVideo(openingTime, movieFilePath, movieName);
        MovieAccessCrud.save(recentVideo, VisionPlayerApp.sMoviesSqlHelper.getWritableDatabase());
    }
}
