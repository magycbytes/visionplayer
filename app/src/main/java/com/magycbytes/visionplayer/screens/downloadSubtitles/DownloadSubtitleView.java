package com.magycbytes.visionplayer.screens.downloadSubtitles;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.magycbytes.opensubtitlelibrary.Models.UserResult.Subtitle;
import com.magycbytes.opensubtitlelibrary.Models.UserResult.SubtitleLanguage;
import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.SubtitlesAdapter;

import java.util.Locale;

/**
 * Created by alexandru on 6/26/16.
 */

class DownloadSubtitleView {

    private final Spinner mLanguageSelector;
    private final TextView mNoSubtitleText;
    private final RecyclerView mSubtitleList;
    private final ProgressBar mExecutingProgress;

    public DownloadSubtitleView(View rootView) {
        mLanguageSelector = (Spinner) rootView.findViewById(R.id.languageSelectorSpinner);
        mNoSubtitleText = (TextView) rootView.findViewById(R.id.noSubtitlesText);
        mSubtitleList = (RecyclerView) rootView.findViewById(R.id.subtitle_list);
        mExecutingProgress = (ProgressBar) rootView.findViewById(R.id.executingProgress);

        mSubtitleList.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
    }

    public void setLanguagesAdapter(SpinnerAdapter adapter) {
        mLanguageSelector.setAdapter(adapter);
    }

    public void setSubtitleAdapter(RecyclerView.Adapter adapter) {
        mSubtitleList.setAdapter(adapter);
        hideProgressBar();
    }

    private void showNoSubtitlesText() {
        boolean toHideInscription = mSubtitleList.getAdapter() != null && mSubtitleList.getAdapter().getItemCount() > 0;
        mNoSubtitleText.setVisibility(
                toHideInscription
                        ? View.INVISIBLE
                        : View.VISIBLE);
    }

    public void setLanguageChangeListener(AdapterView.OnItemSelectedListener listener) {
        mLanguageSelector.setOnItemSelectedListener(listener);
    }

    public void moveToAllLanguagesItem() {
        mLanguageSelector.setSelection(0);
    }

    public void refreshSubtitleAdapter() {
        mSubtitleList.getAdapter().notifyDataSetChanged();
    }

    public Subtitle getSubtitle(int position) {
        return ((SubtitlesAdapter) mSubtitleList.getAdapter()).getSubtitle(position);
    }

    public void showProgressBar() {
        mExecutingProgress.setVisibility(View.VISIBLE);
        mNoSubtitleText.setVisibility(View.INVISIBLE);
    }

    public void hideProgressBar() {
        mExecutingProgress.setVisibility(View.GONE);
        showNoSubtitlesText();
    }

    public String getCurrentLanguageCode() {
        if (mLanguageSelector == null || mLanguageSelector.getAdapter() == null) {
            return Locale.getDefault().getLanguage();
        }
        SubtitleLanguage subtitle = (SubtitleLanguage) mLanguageSelector.getAdapter().getItem(mLanguageSelector.getSelectedItemPosition());
        return subtitle.getIso639();
    }

}
