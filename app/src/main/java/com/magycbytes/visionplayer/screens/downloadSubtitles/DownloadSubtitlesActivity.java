package com.magycbytes.visionplayer.screens.downloadSubtitles;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.magycbytes.visionplayer.Models.CurrentMovie;
import com.magycbytes.visionplayer.R;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.ApiCaller;

public class DownloadSubtitlesActivity extends AppCompatActivity {

    private static final String ARE_NEW_SUBTITLES_DOWNLOADED = "areNewSubtitlesDownloaded";
    private static final String MOVIE_INFO = "movieInfo";
    private ApiCaller mApiCaller;

    public static void show(Context context, CurrentMovie currentMovie) {
        Intent intent = new Intent(context, DownloadSubtitlesActivity.class);
        intent.putExtra(MOVIE_INFO, currentMovie);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_subtitles);

        View rootView = findViewById(R.id.rootLayout);

        CurrentMovie currentMovie = getIntent().getParcelableExtra(MOVIE_INFO);

        DownloadSubtitleView downloadSubtitleView = new DownloadSubtitleView(rootView);
        DownloadSubtitlePresenter downloadSubtitlePresenter = new DownloadSubtitlePresenter(this, downloadSubtitleView);

        mApiCaller = new ApiCaller(rootView, downloadSubtitlePresenter, currentMovie);
        mApiCaller.executeTask(ApiCaller.TASK_GET_LANGUAGES);
        mApiCaller.executeTask(ApiCaller.TASK_LOGIN);

        downloadSubtitlePresenter.setApiCaller(mApiCaller);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.download_subtitle_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.updateSubtitles:
                mApiCaller.executeTask(ApiCaller.TASK_SEARCH_SUBTITLES);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(ARE_NEW_SUBTITLES_DOWNLOADED, mApiCaller.isNewSubtitleDownloaded());
        setResult(RESULT_OK, resultIntent);
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(ARE_NEW_SUBTITLES_DOWNLOADED, mApiCaller.isNewSubtitleDownloaded());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mApiCaller.setNewSubtitleDownloaded(savedInstanceState.getBoolean(ARE_NEW_SUBTITLES_DOWNLOADED));
    }
}

