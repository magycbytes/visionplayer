package com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.downloadSubtitle;

import android.content.Context;

import java.io.File;

/**
 * Created by alexandru on 01/07/16.
 */
public class NameGenerator {

    public static String generate(Context context, String originalName) {
        int counter = 1;

        String newName = originalName;
        while (isFileExist(context, newName)) {
            newName = String.valueOf(counter++) + "." + originalName;
        }
        return newName;
    }

    private static boolean isFileExist(Context context, String targetSubtitleName) {
        File file = new File(context.getFilesDir(), targetSubtitleName);
        return file.exists();
    }
}
