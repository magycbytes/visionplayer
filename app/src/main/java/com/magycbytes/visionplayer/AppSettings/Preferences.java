package com.magycbytes.visionplayer.AppSettings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by alexandru on 6/11/16.
 */
public class Preferences {

    private static final int DEFAULT_ITEM = 0;

    private static final String VIDEO_COLLECTION_LAYOUT = "VideoCollectionLayout";
    private static final String SELECTED_PRESENTING_LAYOUT = "SelectedPresentingLayout";

    public static int getLayoutVideoCollection(Context activity) {
        SharedPreferences sharedPreferences =
                activity.getSharedPreferences(VIDEO_COLLECTION_LAYOUT, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(SELECTED_PRESENTING_LAYOUT, DEFAULT_ITEM);
    }

    public static void saveVideoCollectionLayout(Activity activity, int value) {
        SharedPreferences preferences =
                activity.getSharedPreferences(VIDEO_COLLECTION_LAYOUT, Context.MODE_PRIVATE);
        preferences.edit().putInt(SELECTED_PRESENTING_LAYOUT, value).apply();
    }
}
