package com.magycbytes.visionplayer;

import com.magycbytes.visionplayer.Database.MovieInfoDb;
import com.magycbytes.visionplayer.Database.DatabaseReader;
import com.magycbytes.visionplayer.Models.DbSubtitle;

import java.util.List;

/**
 * Created by tyranul on 3/8/16.
 */
public class MovieInformationExtractor {
    private final DatabaseReader databaseReader;
    private final String targetMovieHash;

    private int lastMoviePosition;
    private List<DbSubtitle> movieSubtitles;

    public MovieInformationExtractor(DatabaseReader databaseReader, String targetMovieHash) {
        this.databaseReader = databaseReader;
        this.targetMovieHash = targetMovieHash;
    }

    public void extractLastMoviePosition() {
        lastMoviePosition = databaseReader.getLastMoviePosition(targetMovieHash);
    }

    public void extractMovieSubtitles() {
        String movieID = extractMovieIDAsString();
        movieSubtitles = databaseReader.getMovieExistingSubtitles(movieID);
    }

    public MovieInfoDb getExtractedInformation() {
        return new MovieInfoDb(
                targetMovieHash, lastMoviePosition, movieSubtitles
        );
    }

    private String extractMovieIDAsString() {
        int movieID = databaseReader.getMovieId(targetMovieHash);
        return String.valueOf(movieID);
    }
}
