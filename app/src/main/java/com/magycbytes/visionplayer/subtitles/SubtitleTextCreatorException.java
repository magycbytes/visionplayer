package com.magycbytes.visionplayer.subtitles;

/**
 * Created by tyranul on 3/14/16.
 */
class SubtitleTextCreatorException extends Exception {
    public SubtitleTextCreatorException(String message) {
        super(message);
    }
}
