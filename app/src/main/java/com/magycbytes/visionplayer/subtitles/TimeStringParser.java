package com.magycbytes.visionplayer.subtitles;

/**
 * Created by tyranul on 3/9/16.
 */
class TimeStringParser {
    private final String lineToParse;
    private String leftTime;
    private String rightTime;

    public TimeStringParser(String lineToParse) {
        this.lineToParse = lineToParse;
    }

    public SubtitleText getResultSubtitleTime() {
        findLeftTime();
        findRightTime();
        return new SubtitleText(leftTime, rightTime);
    }

    private void findLeftTime() {
        int firstSpaceIndexFromLeft = lineToParse.indexOf(' ');
        leftTime = lineToParse.substring(0, firstSpaceIndexFromLeft);
    }

    private void findRightTime() {
        int indexOfTimeDivider = lineToParse.indexOf("-->");
        int secondSpaceIndexFromLeft = lineToParse.indexOf(' ', indexOfTimeDivider) + 1;
        rightTime = lineToParse.substring(secondSpaceIndexFromLeft);
    }
}
