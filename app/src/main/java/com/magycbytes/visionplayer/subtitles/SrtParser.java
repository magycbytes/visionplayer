package com.magycbytes.visionplayer.subtitles;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by magyc Bytes on 16.02.2016.
 */
public class SrtParser {

    public static List<SubtitleText> parse(String absoluteFilePath, String fileEncoding) throws IOException {
        SubtitleTextCreator subtitleTextCreator = new SubtitleTextCreator();

        InputStream inputStream = new FileInputStream(absoluteFilePath);
        Reader reader = new InputStreamReader(inputStream, fileEncoding);

        BufferedReader bufferedReader = new BufferedReader(reader);

        List<SubtitleText> subtitleTextList = new ArrayList<>();

        String readLine;
        while ((readLine = bufferedReader.readLine()) != null) {
            if (readLine.isEmpty()) {
                try {
                    subtitleTextList.add(subtitleTextCreator.getResultSubtitleText());
                } catch (SubtitleTextCreatorException e) {
                    Log.e(SrtParser.class.getSimpleName(), e.getMessage());
                }
                continue;
            }
            if (readLine.contains("-->")) {
                subtitleTextCreator.appendTimeLine(readLine);
                continue;
            }
            subtitleTextCreator.saveReadStringToSubtitle(readLine);
        }
        bufferedReader.close();
        return subtitleTextList;
    }


}
