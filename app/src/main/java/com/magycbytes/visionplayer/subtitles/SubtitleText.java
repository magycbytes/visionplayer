package com.magycbytes.visionplayer.subtitles;

import com.magycbytes.visionplayer.utilities.TimeUtilities;

/**
 * Created by magyc Bytes on 16.02.2016.
 */
public class SubtitleText {
    private final String startTime;
    private final String endTime;
    private String phraseText;

    public SubtitleText(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getPhraseText() {
        return phraseText;
    }

    public void setPhraseText(String phraseText) {
        this.phraseText = phraseText;
    }

    public long getStartTimeInMilliseconds() {
        return TimeUtilities.getTimeIntervalAsMilliseconds(startTime);
    }

    public long getEndTimeInMilliseconds() {
        return TimeUtilities.getTimeIntervalAsMilliseconds(endTime);
    }
}
