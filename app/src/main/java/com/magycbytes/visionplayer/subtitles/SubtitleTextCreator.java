package com.magycbytes.visionplayer.subtitles;

/**
 * Created by tyranul on 3/9/16.
 */
class SubtitleTextCreator {
    private SubtitleText resultSubtitleText;
    private StringBuilder stringBuilder;

    public void appendTimeLine(String timeLine) {
        TimeStringParser timeStringParser = new TimeStringParser(timeLine);
        resultSubtitleText = timeStringParser.getResultSubtitleTime();
    }

    public void saveReadStringToSubtitle(String text) {
        if (stringBuilder == null) {
            stringBuilder = new StringBuilder();
        } else {
            stringBuilder.append("\n");
            stringBuilder.append(text);
        }
    }

    public SubtitleText getResultSubtitleText() throws SubtitleTextCreatorException {
        if (resultSubtitleText == null) {
            throw new SubtitleTextCreatorException("No time for subtitle was set");
        }
        if (stringBuilder == null) {
            throw new SubtitleTextCreatorException("No subtitle text was added");
        }
        resultSubtitleText.setPhraseText(stringBuilder.toString());
        stringBuilder = null;

        return resultSubtitleText;
    }
}
