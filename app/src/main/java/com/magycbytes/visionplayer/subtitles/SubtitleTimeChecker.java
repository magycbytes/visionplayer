package com.magycbytes.visionplayer.subtitles;

/**
 * Created by tyranul on 3/13/16.
 */
public class SubtitleTimeChecker {
    private final long currentVideoPosition;
    private long startTimeCurrentSubtitle;
    private long endTimeCurrentSubtitle;
    private long nextSubtitleStartTime;

    public SubtitleTimeChecker(long videoPosition) {
        currentVideoPosition = videoPosition;
    }

    public void setCurrentSubtitle(SubtitleText currentSubtitle) {
        startTimeCurrentSubtitle = currentSubtitle.getStartTimeInMilliseconds();
        endTimeCurrentSubtitle = currentSubtitle.getEndTimeInMilliseconds();

        nextSubtitleStartTime = -1;
    }

    public void setNextSubtitle(SubtitleText nextTimeSubtitle) {
        nextSubtitleStartTime = nextTimeSubtitle.getStartTimeInMilliseconds();
    }

    public boolean isCurrentSubtitleOld() {
        return endTimeCurrentSubtitle < currentVideoPosition;
    }

    public boolean isNoSubtitleForThisVideoTime() {
        return startTimeCurrentSubtitle > currentVideoPosition;
    }

    public long getNextTimeToCheck() {
        if (nextSubtitleStartTime < 0) {
            return nextSubtitleStartTime;
        }
        return nextSubtitleStartTime - currentVideoPosition;
    }

    public long getNextTimeToCheckForNoSubtitle() {
        return startTimeCurrentSubtitle - currentVideoPosition;
    }

    public long getIntervalToShowSubtitle() {
        return endTimeCurrentSubtitle - currentVideoPosition;
    }
}
