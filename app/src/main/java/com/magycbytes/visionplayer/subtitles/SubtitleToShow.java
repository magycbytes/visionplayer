package com.magycbytes.visionplayer.subtitles;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * Created by magyc Bytes on 18.02.2016.
 */
public class SubtitleToShow {
    private String phraseTextToShowNow;
    private long nextTimeToCheckForSubtitle;
    private long timeIntervalToShowCurrentSubtitle;

    public SubtitleToShow() {
        phraseTextToShowNow = "";
        nextTimeToCheckForSubtitle = -1;
        timeIntervalToShowCurrentSubtitle = -1;
    }

    public Spanned getPhraseTextToShowNow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(phraseTextToShowNow, Html.FROM_HTML_MODE_COMPACT);
        }
        return Html.fromHtml(phraseTextToShowNow);
    }

    public void setPhraseText(String phraseTextToShowNow) {
        this.phraseTextToShowNow = phraseTextToShowNow;
    }

    public long getNextTimeToCheckForSubtitle() {
        return nextTimeToCheckForSubtitle;
    }

    public void setNextTimeToCheck(long nextTimeToCheckForSubtitle) {
        this.nextTimeToCheckForSubtitle = nextTimeToCheckForSubtitle;
    }

    public long getTimeIntervalToShowCurrentSubtitle() {
        return timeIntervalToShowCurrentSubtitle;
    }

    public void setIntervalToShow(long timeIntervalToShowCurrentSubtitle) {
        this.timeIntervalToShowCurrentSubtitle = timeIntervalToShowCurrentSubtitle;
    }
}
