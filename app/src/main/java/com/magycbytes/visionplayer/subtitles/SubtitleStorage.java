package com.magycbytes.visionplayer.subtitles;

import java.util.List;

/**
 * Created by tyranul on 3/12/16.
 */
class SubtitleStorage {
    private List<SubtitleText> subtitleText;

    public SubtitleStorage(List<SubtitleText> subtitleText) {
        if (subtitleText == null) {
            throw new IllegalArgumentException("subtitleText");
        }
        this.subtitleText = subtitleText;
    }

    public SubtitleToShow getSubtitleAtIndicatedTime(long videoPosition) {
        if (videoIsNotStarted(videoPosition)) {
            SubtitleToShow subtitleToShow = new SubtitleToShow();
            subtitleToShow.setNextTimeToCheck(100);
            return subtitleToShow;
        }

        int nextSubtitleIndex = 0;

        SubtitleTimeChecker subtitleTimeChecker = new SubtitleTimeChecker(videoPosition);
        for (SubtitleText currentSubtitle : subtitleText) {
            nextSubtitleIndex++;
            subtitleTimeChecker.setCurrentSubtitle(currentSubtitle);

            if (nextSubtitleIndex < subtitleText.size()) {
                subtitleTimeChecker.setNextSubtitle(subtitleText.get(nextSubtitleIndex));
            }

            if (subtitleTimeChecker.isCurrentSubtitleOld()) {
                continue;
            }

            SubtitleToShow subtitleToShow = new SubtitleToShow();

            if (subtitleTimeChecker.isNoSubtitleForThisVideoTime()) {
                subtitleToShow.setNextTimeToCheck(subtitleTimeChecker.getNextTimeToCheckForNoSubtitle());
            } else {
                subtitleToShow.setNextTimeToCheck(subtitleTimeChecker.getNextTimeToCheck());
                subtitleToShow.setIntervalToShow(subtitleTimeChecker.getIntervalToShowSubtitle());

                subtitleToShow.setPhraseText(currentSubtitle.getPhraseText());
            }

            return subtitleToShow;
        }
        return null;
    }

    private boolean videoIsNotStarted(long videoCurrentPosition) {
        return videoCurrentPosition < 1;
    }

}
