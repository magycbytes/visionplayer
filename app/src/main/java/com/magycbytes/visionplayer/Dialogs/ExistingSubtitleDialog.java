package com.magycbytes.visionplayer.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.magycbytes.opensubtitlelibrary.R;
import com.magycbytes.visionplayer.screens.downloadSubtitles.Helpers.ApiCaller;

/**
 * Created by tyranul on 3/7/16.
 */
public class ExistingSubtitleDialog {
    private AlertDialog alertDialog;
    private final Context mContext;
    private final ApiCaller mApiCaller;

    public ExistingSubtitleDialog(Context context, ApiCaller apiCaller) {
        mContext = context;
        mApiCaller = apiCaller;
    }

    public void showMessageAlreadyExistSubtitle() {
        if (alertDialog == null) {
            createDialog();
        }
        alertDialog.show();
    }

    private void createDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setMessage(R.string.message_download_one_more_time);
        dialogBuilder.setTitle(R.string.title_existing_subtitle);
        dialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mApiCaller.executeTask(ApiCaller.TASK_DOWNLOAD_SUBTITLE);
            }
        });
        dialogBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogBuilder.setCancelable(true);
        alertDialog = dialogBuilder.create();
    }
}
