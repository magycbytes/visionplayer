package com.magycbytes.visionplayer.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alexandru on 6/26/16.
 */

public class CurrentMovie implements Parcelable {

    public static final Creator<CurrentMovie> CREATOR = new Creator<CurrentMovie>() {
        @Override
        public CurrentMovie createFromParcel(Parcel in) {
            return new CurrentMovie(in);
        }

        @Override
        public CurrentMovie[] newArray(int size) {
            return new CurrentMovie[size];
        }
    };
    
    private final String mMovieHash;
    private final String mMovieLocation;
    private final Double mMovieSize;

    public CurrentMovie(String movieHash, String movieLocation, Double movieSize) {
        mMovieHash = movieHash;
        mMovieLocation = movieLocation;
        mMovieSize = movieSize;
    }

    private CurrentMovie(Parcel in) {
        mMovieHash = in.readString();
        mMovieLocation = in.readString();
        mMovieSize = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mMovieHash);
        parcel.writeString(mMovieLocation);
        parcel.writeDouble(mMovieSize);
    }

    public String getMovieHash() {
        return mMovieHash;
    }

    public Double getMovieSize() {
        return mMovieSize;
    }

}
