package com.magycbytes.visionplayer.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alexandru on 29/06/16.
 */
public class DbSubtitle implements Parcelable {

    public static final Creator<DbSubtitle> CREATOR = new Creator<DbSubtitle>() {
        @Override
        public DbSubtitle createFromParcel(Parcel in) {
            return new DbSubtitle(in);
        }

        @Override
        public DbSubtitle[] newArray(int size) {
            return new DbSubtitle[size];
        }
    };
    private final String mSubtitleServerId;
    private final String mSubtitleLanguageIso;
    private final String mSubtitleEncoding;
    private final String mFileName;

    public DbSubtitle(String subtitleServerId, String subtitleLanguageIso, String subtitleEncoding, String fileName) {
        mSubtitleServerId = subtitleServerId;
        mSubtitleLanguageIso = subtitleLanguageIso;
        mSubtitleEncoding = subtitleEncoding;
        mFileName = fileName;
    }

    private DbSubtitle(Parcel in) {
        mSubtitleServerId = in.readString();
        mSubtitleLanguageIso = in.readString();
        mSubtitleEncoding = in.readString();
        mFileName = in.readString();
    }

    public String getSubtitleServerId() {
        return mSubtitleServerId;
    }

    public String getSubtitleLanguageIso() {
        return mSubtitleLanguageIso;
    }

    public String getEncoding() {
        return mSubtitleEncoding;
    }

    public String getFileName() {
        return mFileName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mSubtitleServerId);
        parcel.writeString(mSubtitleLanguageIso);
        parcel.writeString(mSubtitleEncoding);
        parcel.writeString(mFileName);
    }
}
