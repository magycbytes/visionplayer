package com.magycbytes.visionplayer.tools;

import java.io.File;

/**
 * Created by tyranul on 3/7/16.
 */
public class FileUtilities {

    public static double getFileSize(String filePath) {
        File videoFile = new File(filePath);
        return videoFile.length();
    }

}
