package com.magycbytes.visionplayer;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.magycbytes.visionplayer.Database.DatabaseMoviesSqlHelper;

import io.fabric.sdk.android.Fabric;

/**
 * Created by tyranul on 8/27/16.
 */

public class VisionPlayerApp extends Application {

    public static DatabaseMoviesSqlHelper sMoviesSqlHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        sMoviesSqlHelper = new DatabaseMoviesSqlHelper(this);
        initCrashlytics();

//        if (BuildConfig.DEBUG) {
//            Configuration configuration = new Configuration(getResources().getConfiguration());
//            Locale locale = new Locale("ja");
//            Locale.setDefault(locale);
//            configuration.locale = locale;
//            getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
//        }
    }

    @Override
    public void onTerminate() {
        if (sMoviesSqlHelper != null) sMoviesSqlHelper.close();

        super.onTerminate();
    }

    private void initCrashlytics() {
        CrashlyticsCore core = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        Fabric.with(this, new Crashlytics.Builder().core(core).build());
    }
}
